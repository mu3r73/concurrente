package monitores.ej05_lock_cond;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class Impresoras {
	
	private Lock lock;
	private Condition cHayLibre;
	
	private boolean[] libres;
	private int cantLibres;

	public Impresoras(int cant) {
		super();
		this.lock = new ReentrantLock();
		this.cHayLibre = lock.newCondition();
		
		this.cantLibres = cant;
		this.libres = new boolean[cant];
		for (int i = 0; i < this.libres.length; i++) {
			this.libres[i] = true;
		}
	}
	
	public int pedir() {
		int num = -1;
		try {
			this.lock.lock();
			while (this.cantLibres <= 0) {
				Padding.mostrarMsgDeThreadActual("no hay impresora libre");
				this.cHayLibre.await();
			}
			this.cantLibres--;
			num = this.findLibre();
			this.libres[num] = false;
			Padding.mostrarMsgDeThreadActual("obtiene impresora " + num);
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
		return num;
	}
	
	public void liberar(int num) {
		this.lock.lock();
		if (num < this.libres.length) {
			this.cantLibres++;
			this.libres[num] = true;
			Padding.mostrarMsgDeThreadActual("libera impresora" + num);
			this.cHayLibre.signalAll();
		}
		this.lock.unlock();
	}
	
	public int findLibre() {
		for (int i = 0; i < this.libres.length; i++) {
			if (this.libres[i]) {
				return i;
			}
		}
		return -1;
	}
	
}
