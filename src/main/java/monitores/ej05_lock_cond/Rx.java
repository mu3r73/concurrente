package monitores.ej05_lock_cond;

import gen.Padding;

public class Rx implements Runnable {
	
	private Impresoras ismon;
	
	public Rx(Impresoras ismon) {
		super();
		this.ismon = ismon;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("solicita impresora");
			int num = this.ismon.pedir();
			
			Padding.mostrarMsgDeThreadActual("usa impresora" + num);
			Thread.sleep(1000);
			Padding.mostrarMsgDeThreadActual("termina de usar impresora" + num);
			
			this.ismon.liberar(num);
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
