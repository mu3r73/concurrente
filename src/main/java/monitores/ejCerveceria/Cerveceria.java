package monitores.ejCerveceria;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class Cerveceria {
	
	private Lock lock;
	private Condition cHayVacios;
	private Condition cHayLlenos;
	private Condition cChopperaLibre;
	
	private int totalVasos;
	private int cantLlenos;
	private boolean llenando;
	
	public Cerveceria(int totalVasos) {
		super();
		this.totalVasos = totalVasos;
		this.cantLlenos = 0;
		this.llenando = false;
		
		this.lock = new ReentrantLock();
		this.cHayVacios = lock.newCondition();
		this.cHayLlenos = lock.newCondition();
		this.cChopperaLibre = lock.newCondition();
	}
	
	public void llenarVaso() {
		try {
			this.lock.lock();
			while (this.cantLlenos == this.totalVasos) {
				Padding.mostrarMsgDeThreadActual("no hay vasos vacíos");
				this.cHayVacios.await();
			}
			Padding.mostrarMsgDeThreadActual("obtiene vaso vacío");
			while (this.llenando) {
				Padding.mostrarMsgDeThreadActual("choppera ocupada");
				this.cChopperaLibre.await();
			}
			Padding.mostrarMsgDeThreadActual("obtiene choppera");
			this.llenando = true;
			Padding.mostrarMsgDeThreadActual("llena un vaso");
			
			Padding.mostrarMsgDeThreadActual("vaso lleno");
			this.llenando = false;
			Padding.mostrarMsgDeThreadActual("libera choppera");
			this.cChopperaLibre.signalAll();
			
			this.cantLlenos++;
			Padding.mostrarMsgDeThreadActual("libera vaso lleno");
			this.cHayLlenos.signalAll();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
	public void vaciarVaso() {
		try {
			this.lock.lock();
			while (this.cantLlenos == 0) {
				Padding.mostrarMsgDeThreadActual("no hay vasos llenos");
				this.cHayLlenos.await();
			}
			Padding.mostrarMsgDeThreadActual("obtiene vaso lleno");
			this.cantLlenos--;
			Padding.mostrarMsgDeThreadActual("vacía vaso");
			Padding.mostrarMsgDeThreadActual("libera vaso vacío");
			this.cHayVacios.signalAll();
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
}
