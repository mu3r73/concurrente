package monitores.ejCerveceria;

import java.util.Random;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Random rng = new Random();
		
		Cerveceria cmon = new Cerveceria(5);
		
		for (int i = 0; i < 2; i++) {
			new Thread(new RBarman(cmon, 3), Padding.mkNombreConPadding("barman", i)).start();
		}
		for (int i = 0; i < 4 + rng.nextInt(2); i++) {
			new Thread(new RCliente(cmon), Padding.mkNombreConPadding("cliente", i)).start();
		}
	}
	
}
