package monitores.ejCerveceria;

import gen.Padding;

public class RCliente implements Runnable {
	
	private Cerveceria cmon;
	
	public RCliente(Cerveceria cmon) {
		super();
		this.cmon = cmon;
	}

	@Override
	public void run() {
		Padding.mostrarMsgDeThreadActual("quiere un vaso");
		this.cmon.vaciarVaso();
	}

}
