package monitores.ej01_lock_cond;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

public class Monitor {
	
	private int cantThreads = 0;
	
	private Lock lock = new ReentrantLock();
	private Condition cMasDe2 = lock.newCondition();
	
	public void entrada() {
		try {
			this.lock.lock();
			this.cantThreads++;
			if (this.cantThreads < 3) {
				Padding.mostrarMsgDeThreadActual("se suspende");
				this.cMasDe2.await();
			} else {
				Padding.mostrarMsgDeThreadActual("despierta a todas");
				this.cMasDe2.signalAll();
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
}
