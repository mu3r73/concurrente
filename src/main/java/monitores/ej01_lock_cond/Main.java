package monitores.ej01_lock_cond;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Monitor mon = new Monitor();
		for (int i = 0; i < 3; i++) {
			new Thread(new Rx(mon), Padding.mkNombreConPadding("thread", i)).start();
		}
	}
	
}
