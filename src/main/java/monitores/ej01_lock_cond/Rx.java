package monitores.ej01_lock_cond;

import gen.Padding;

public class Rx implements Runnable {

	private Monitor mon;

	public Rx(Monitor mon) {
		super();
		this.mon = mon;
	}

	@Override
	public void run() {
		Padding.mostrarMsgDeThreadActual("llama a entrada");
		this.mon.entrada();
		Padding.mostrarMsgDeThreadActual("se despierta");
	}

}
