package monitores.ej01_primitivas;

import gen.Padding;

public class Auxiliar {

	private int cantLlam = 0;
	
	public synchronized void entrada() throws InterruptedException {
		this.cantLlam++;
		if (this.cantLlam < 3) {
			Padding.mostrarMsgDeThreadActual("se suspende");
			this.wait();
		} else {
			Padding.mostrarMsgDeThreadActual("despierta a todas");
			this.notifyAll();
		}
	}
	
}
