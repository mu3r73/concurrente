package monitores.ej01_primitivas;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Auxiliar aux = new Auxiliar();
		for (int i = 0; i < 3; i++) {
			new Thread(new Rx(aux), Padding.mkNombreConPadding("thread", i)).start();
		}
	}
	
}
