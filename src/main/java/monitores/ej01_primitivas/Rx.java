package monitores.ej01_primitivas;

import gen.Padding;

public class Rx implements Runnable {

	private Auxiliar aux;
	
	public Rx(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		try {
		Padding.mostrarMsgDeThreadActual("llama a entrada");
			this.aux.entrada();
			Padding.mostrarMsgDeThreadActual("se despierta");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
