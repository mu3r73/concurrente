package monitores.productores_consumidores;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class BufferCompartido {

	private Item[] buffer;
	private int capacidad;
	private int cantItems;
	private int posEncolar;
	private int posDesencolar;
	
	private Lock lock;
	private Condition cNoLleno;
	private Condition cNoVacio;
	
	public BufferCompartido(int capacidad) {
		super();
		this.buffer = new Item[capacidad];
		this.capacidad = capacidad;
		this.cantItems = 0;
		this.posEncolar = 0;
		this.posDesencolar = 0;
		
		this.lock =  new ReentrantLock();
		this.cNoLleno = this.lock.newCondition();
		this.cNoVacio = this.lock.newCondition();
	}
	
	public void insertar(Item item) {
		try {
			this.lock.lock();
			while (this.cantItems == this.capacidad) {
				// espero que el buffer no esté lleno
				this.cNoLleno.await();
			}
			this.buffer[this.posEncolar] = item;
			this.posEncolar = (this.posEncolar + 1) % this.capacidad;
			this.cantItems++;
			// aviso que el buffer no está vacío, por si alguien está esperando
			this.cNoVacio.signal();
			Padding.mostrarMsgDeThreadActual("insertó" + item.getNombre());
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
	public Item extraer() {
		Item item = null;
		try {
			this.lock.lock();
			while (this.cantItems == 0) {
				// espero que el buffer no esté vacío
				this.cNoVacio.await();
			}
			item = this.buffer[this.posDesencolar];
			this.posDesencolar = (this.posDesencolar + 1) % this.capacidad;
			this.cantItems--;
			// aviso que el buffer no está lleno, por si alguien está esperando
			this.cNoLleno.signal();
			Padding.mostrarMsgDeThreadActual("extrajo" + item.getNombre());
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
		return item;
	}
	
}
