package monitores.productores_consumidores;

public class Item {
	
	private String nombre;

	public Item(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return this.nombre;
	}

}
