package monitores.productores_consumidores;

public class RProductor implements Runnable {
	
	private BufferCompartido bcmon;
	private Item item;
	
	public RProductor(BufferCompartido bcmon, Item item) {
		super();
		this.bcmon = bcmon;
		this.item = item;
	}

	@Override
	public void run() {
		this.bcmon.insertar(item);
	}

}
