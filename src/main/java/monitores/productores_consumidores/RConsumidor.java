package monitores.productores_consumidores;

public class RConsumidor implements Runnable {
	
	private BufferCompartido bcmon;

	public RConsumidor(BufferCompartido bcmon) {
		super();
		this.bcmon = bcmon;
	}

	@Override
	public void run() {
		this.bcmon.extraer();
	}
	
}
