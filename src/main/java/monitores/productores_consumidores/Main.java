package monitores.productores_consumidores;

import java.util.Random;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		final int tamBuffer = 5;
		BufferCompartido bcmon = new BufferCompartido(tamBuffer);

		Random rng = new Random();

		// entre 4 y 9 productores
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {
			new Thread(new RProductor(bcmon, new Item("item" + i)), Padding.mkNombreConPadding("productor", i)).start();
		}
		// entre 4 y 9 consumidores
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {
			new Thread(new RConsumidor(bcmon), Padding.mkNombreConPadding("consumidor", i)).start();
		}
	}

}
