package monitores.ej03;

import java.util.Random;

import gen.Padding;

/*
 * ojo: no termina si el frasco se queda sin bolitas y sólo hay jugadores esperando para sacar
 */
public class Main {
	static Random rng = new Random();

	private static Runnable generarJugador(Frasco frascoMon) {
		if (rng.nextDouble() < .6) {	// ~60% de los jugadores quieren poner bolitas
			return new RJugador(frascoMon, 3 + rng.nextInt(5), 0);	// va a poner entre 3 y 7 bolitas
		} else {
			return new RJugador(frascoMon, 0, 3 + rng.nextInt(4));	// va a sacar entre 3 y 6 bolitas
		}
	}
	
	public static void main(String[] args) {
		Frasco frascoMon = new Frasco();
		
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {	// entre 4 y 9 jugadores  
			new Thread(generarJugador(frascoMon), Padding.mkNombreConPadding("jugador", i)).start();
		}
	}

}
