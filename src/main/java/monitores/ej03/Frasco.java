package monitores.ej03;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class Frasco {
	
	private Lock lock = new ReentrantLock();
	private Condition cFaltanBolitas = lock.newCondition();
	
	private int totalBolitas = 0;
	
	public void poner(int cant) {
		this.lock.lock();
		this.totalBolitas += cant;
		Padding.mostrarMsgDeThreadActual("puso " + cant + " bolitas");
		Padding.mostrarMsgDeThreadActual("quedan " + this.totalBolitas + " bolitas en en frasco");
		this.cFaltanBolitas.signalAll();
		this.lock.unlock();
	}
	
	public void sacar(int cant) {
		try {
			this.lock.lock();
			while (cant > this.totalBolitas) {
				Padding.mostrarMsgDeThreadActual("no hay suficientes bolitas");
				this.cFaltanBolitas.await();
			}
			this.totalBolitas -= cant;
			Padding.mostrarMsgDeThreadActual("sacó " + cant + " bolitas");
			Padding.mostrarMsgDeThreadActual("quedan " + this.totalBolitas + " bolitas en el frasco");
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
}
