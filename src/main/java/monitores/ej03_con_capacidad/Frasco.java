package monitores.ej03_con_capacidad;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class Frasco {
	
	private int capacidad;
	private Lock lock;
	private Condition cFaltanBolitas;
	private Condition cFrascoLleno;
	
	private int totalBolitas;
	
	public Frasco(int capacidad) {
		super();
		this.capacidad = capacidad;
		this.lock = new ReentrantLock();
		this.cFaltanBolitas = lock.newCondition();
		this.cFrascoLleno = lock.newCondition();
		this.totalBolitas = 0;
	}

	public void poner(int cant) {
		try {
			this.lock.lock();
			while (this.totalBolitas + cant > this.capacidad) {
			Padding.mostrarMsgDeThreadActual("no hay espacio suficiente");
				this.cFrascoLleno.await();
			}
			this.totalBolitas += cant;
			Padding.mostrarMsgDeThreadActual("puso " + cant + " bolitas");
			Padding.mostrarMsgDeThreadActual("hay " + this.totalBolitas + " bolitas en el frasco");
			this.cFaltanBolitas.signalAll();
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}			
	}
	
	public void sacar(int cant) {
		try {
			this.lock.lock();
			while (cant > this.totalBolitas) {
				Padding.mostrarMsgDeThreadActual("no hay suficientes bolitas");
				this.cFaltanBolitas.await();
			}
			this.totalBolitas -= cant;
			Padding.mostrarMsgDeThreadActual("sacó " + cant + " bolitas");
			Padding.mostrarMsgDeThreadActual("hay " + this.totalBolitas + " bolitas en el frasco");
			this.cFrascoLleno.signalAll();
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
}
