package monitores.ej03_con_capacidad;

import gen.Padding;

public class RJugador implements Runnable {
	
	private Frasco frascoMon;

	private int cantPoner;
	private int cantSacar;
	
	public RJugador(Frasco frascoMon, int cantPoner, int cantSacar) {
		super();
		this.frascoMon = frascoMon;
		this.cantPoner = cantPoner;
		this.cantSacar = cantSacar;
	}

	@Override
	public void run() {
		if (this.cantPoner > 0) {
			Padding.mostrarMsgDeThreadActual("quiere poner " + this.cantPoner + " bolitas");
			this.frascoMon.poner(cantPoner);
		}
		
		if (this.cantSacar > 0) {
			Padding.mostrarMsgDeThreadActual("quiere sacar " + this.cantSacar + " bolitas");
			this.frascoMon.sacar(cantSacar);
		}
	}

}
