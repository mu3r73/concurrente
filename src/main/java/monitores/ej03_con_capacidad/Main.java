package monitores.ej03_con_capacidad;

import java.util.Random;

import gen.Padding;

/*
 * frasco con capacidad limitada
 * ojo: no termina si el frasco se queda sin bolitas y sólo hay jugadores esperando para sacar
 * ojo: no termina si el frasco está lleno y sólo hay jugadores esperando para poner
 */
public class Main {
	static Random rng = new Random();

	private static Runnable generarJugador(Frasco frascoMon) {
		if (rng.nextBoolean()) {
			return new RJugador(frascoMon, 3 + rng.nextInt(5), 0);	// va a poner entre 3 y 7 bolitas
		} else {
			return new RJugador(frascoMon, 0, 3 + rng.nextInt(5));	// va a sacar entre 3 y 7 bolitas
		}
	}
	
	public static void main(String[] args) {
		Frasco frascoMon = new Frasco(15);	// frasco con espacio para 15 bolitas
		
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {	// entre 4 y 9 jugadores  
			new Thread(generarJugador(frascoMon), Padding.mkNombreConPadding("jugador", i)).start();
		}
	}

}
