package monitores.filosofos;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class Compartidos {
	
	private int cantTotal;
	private Estado[] estado;
	
	private Lock lock;
	private Condition[] cPuedeComer;
	
	public Compartidos(int cantTotal) {
		super();
		this.cantTotal = cantTotal;
		
		this.estado = new Estado[cantTotal];
		for (int i = 0; i < cantTotal; i++) {
			this.estado[i] = Estado.PENSANDO;
		}

		this.lock = new ReentrantLock();
		this.cPuedeComer = new Condition[cantTotal];
		for (int i = 0; i < cantTotal; i++) {
			this.cPuedeComer[i] = this.lock.newCondition();
		}
	}
	
	public void obtenerPalillos(int num) {
		try {
			this.lock.lock();
			estado[num] = Estado.HAMBRIENTO;
			while (!this.puedeComer(num)) {
				Padding.mostrarMsgDeThreadActual("esperando palillos");
				this.cPuedeComer[num].await();
			}
			Padding.mostrarMsgDeThreadActual("obtiene palillos");
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
	public void liberarPalillos(int num) {
		this.lock.lock();
		this.estado[num] = Estado.PENSANDO;
		Padding.mostrarMsgDeThreadActual("libera palillos");

		if (this.puedeComer(this.numAnterior(num))) {
			this.cPuedeComer[this.numAnterior(num)].signal();
		}
		if (this.puedeComer(this.numSiguiente(num))) {
			this.cPuedeComer[this.numSiguiente(num)].signal();
		}
		
		this.lock.unlock();
	}
	
	private boolean puedeComer(int num) {
		return estado[this.numAnterior(num)] != Estado.COMIENDO
				&& estado[this.numSiguiente(num)] != Estado.COMIENDO
				&& estado[num] == Estado.HAMBRIENTO;
	}
	
	private int numAnterior(int num) {
		return (num + this.cantTotal - 1) % this.cantTotal;
	}
	
	private int numSiguiente(int num) {
		return (num + 1) % this.cantTotal;
	}

}
