package monitores.filosofos;

import gen.Padding;

/*
 * ojo: si los filósofos corren en loop infinito, nunca termina
 */
public class Main {
	
	public static void main(String[] args) {
		Compartidos cmon = new Compartidos(5);
		
		for (int i = 0; i < 5; i++) {
			new Thread(new RFilosofo(i, cmon), Padding.mkNombreConPadding("filósofo", i)).start();
		}
	}
	
}
