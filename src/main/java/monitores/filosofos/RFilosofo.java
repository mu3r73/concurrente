package monitores.filosofos;

import gen.Padding;

public class RFilosofo implements Runnable {
	
	private int num;
	private Compartidos cmon;
	
	public RFilosofo(int num, Compartidos cmon) {
		super();
		this.num = num;
		this.cmon = cmon;
	}

	@Override
	public void run() {
		try {
			while (true) {
				Padding.mostrarMsgDeThreadActual("piensa");
				Thread.sleep(2000);
				Padding.mostrarMsgDeThreadActual("le da hambre");
				
				this.cmon.obtenerPalillos(this.num);
				
				Padding.mostrarMsgDeThreadActual("empieza a comer");
				Thread.sleep(1000);
				Padding.mostrarMsgDeThreadActual("termina de comer");
				
				this.cmon.liberarPalillos(this.num);
			}
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
