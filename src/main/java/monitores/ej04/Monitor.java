package monitores.ej04;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class Monitor {
	
	private Lock lock;
	private Condition[] cs;
	
	public Monitor() {
		super();
		this.lock = new ReentrantLock();
		this.cs = new Condition[10];
		for (int i = 0; i < cs.length; i++) {
			this.cs[i] = lock.newCondition();
		}
	}
	
	public void bloquear(int n) {
		try {
			this.lock.lock();
			Padding.mostrarMsgDeThreadActual("se bloquea, con orden " + n);
			this.cs[n - 1].await();

		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
	public void liberar() {
		this.lock.lock();
		for (int i = 0; i < 10; i++) {
			Padding.mostrarMsgDeThreadActual("libera a todos los threads de orden " + (i + 1));
			this.cs[i].signalAll();
		}
		this.lock.unlock();
	}
	
}
