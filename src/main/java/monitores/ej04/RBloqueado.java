package monitores.ej04;

import gen.Padding;

public class RBloqueado implements Runnable {
	
	private int orden;
	private Monitor mon;
	
	public RBloqueado(Monitor mon, int orden) {
		super();
		this.orden = orden;
		this.mon = mon;
	}

	@Override
	public void run() {
		this.mon.bloquear(this.orden);
		Padding.mostrarMsgDeThreadActual("liberado (orden " + this.orden + ")");
	}

}
