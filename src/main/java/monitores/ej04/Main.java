package monitores.ej04;

import java.util.Random;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Random rng = new Random();
		
		Monitor mon = new Monitor();
		
		for (int i = 0; i < 22; i++) {	// threads a bloquearse
			new Thread(new RBloqueado(mon, 1 + rng.nextInt(10)), Padding.mkNombreConPadding("bloqueado", i + 1)).start();;
		}
		new Thread(new RLiberador(mon), Padding.mkNombreConPadding("liberador", 0)).start();
	}
	
}
