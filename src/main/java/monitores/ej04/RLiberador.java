package monitores.ej04;

import gen.Padding;

public class RLiberador implements Runnable {
	
	private Monitor mon;
	
	public RLiberador(Monitor mon) {
		super();
		this.mon = mon;
	}

	@Override
	public void run() {
		this.mon.liberar();
		Padding.mostrarMsgDeThreadActual("mischief managed");
	}

}
