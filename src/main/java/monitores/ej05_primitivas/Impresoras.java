package monitores.ej05_primitivas;

import gen.Padding;

public class Impresoras {

	private boolean[] libres;
	private int cantLibres;

	public Impresoras(int cant) {
		super();
		this.cantLibres = cant;
		this.libres = new boolean[cant];
		for (int i = 0; i < this.libres.length; i++) {
			this.libres[i] = true;
		}
	}
	
	public synchronized int pedir() throws InterruptedException {
		int num = -1;
		while (this.cantLibres <= 0) {
			Padding.mostrarMsgDeThreadActual("no hay impresora libre");
			this.wait();
		}
		this.cantLibres--;
		num = this.findLibre();
		this.libres[num] = false;
		Padding.mostrarMsgDeThreadActual("obtiene impresora " + num);
		return num;
	}
	
	public synchronized void liberar(int num) {
		if (num < this.libres.length) {
			this.cantLibres++;
			this.libres[num] = true;
			Padding.mostrarMsgDeThreadActual("libera impresora" + num);
			this.notifyAll();
		}
	}
	
	public int findLibre() {
		for (int i = 0; i < this.libres.length; i++) {
			if (this.libres[i]) {
				return i;
			}
		}
		return -1;
	}

}
