package monitores.ej05_primitivas;

import gen.Padding;

public class Rx implements Runnable {
	
	private Impresoras isaux;
	
	public Rx(Impresoras isaux) {
		super();
		this.isaux = isaux;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("solicita impresora");
			int num = this.isaux.pedir();
			
			Padding.mostrarMsgDeThreadActual("usa impresora" + num);
			Thread.sleep(1000);
			Padding.mostrarMsgDeThreadActual("termina de usar impresora" + num);
			
			this.isaux.liberar(num);
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
