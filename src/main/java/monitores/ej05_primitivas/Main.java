package monitores.ej05_primitivas;

import java.util.Random;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Random rng = new Random();
		
		Impresoras isaux = new Impresoras(3);
		
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {	// entre 4 y 9 threads
			new Thread(new Rx(isaux), Padding.mkNombreConPadding("thread", i)).start();
		}
	}
	
}
