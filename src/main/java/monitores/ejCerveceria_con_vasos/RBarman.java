package monitores.ejCerveceria_con_vasos;

import gen.Padding;

public class RBarman implements Runnable {
	
	private Cerveceria cmon;
	private int quota;
	
	public RBarman(Cerveceria cmon, int quota) {
		super();
		this.cmon = cmon;
		this.quota = quota;
	}

	@Override
	public void run() {
		Padding.mostrarMsgDeThreadActual("hola");
		for (int i = 0; i < quota; i++) {
			Vaso vaso = this.cmon.obtenerVacio();
			this.cmon.llenar(vaso);
			this.cmon.ponerLleno(vaso);
		}
		Padding.mostrarMsgDeThreadActual("chau");
	}

}
