package monitores.ejCerveceria_con_vasos;

import java.util.Arrays;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import gen.Padding;

/*
 * monitor
 */
public class Cerveceria {
	
	private Lock lock;
	private Condition cHayVacios;
	private Condition cHayLlenos;
	private Condition cChopperaLibre;
	
	private Vaso[] estanteLlenos;
	private Vaso[] estanteVacios;
	private int cantLlenos;
	private int cantVacios;
	private boolean llenando;
	
	public Cerveceria(int totalVasos) {
		super();
		this.lock = new ReentrantLock();
		this.cHayVacios = lock.newCondition();
		this.cHayLlenos = lock.newCondition();
		this.cChopperaLibre = lock.newCondition();

		this.estanteLlenos = new Vaso[totalVasos];
		this.estanteVacios = new Vaso[totalVasos];
		for (int i = 0; i < totalVasos; i++) {
			this.estanteLlenos[i] = null;
			this.estanteVacios[i] = new Vaso("vaso" + i);
		}
		this.cantLlenos = 0;
		this.cantVacios = totalVasos;
		this.llenando = false;
	}
	
	public Vaso obtenerVacio() {
		Vaso vaso = null;
		try {
			this.lock.lock();
			while (this.cantVacios == 0) {
				Padding.mostrarMsgDeThreadActual("no hay vasos vacíos en el estante");
				this.cHayVacios.await();
			}
			int num = this.findVaso(this.estanteVacios);
			vaso = this.estanteVacios[num];
			this.estanteVacios[num] = null;
			this.cantVacios--;
			Padding.mostrarMsgDeThreadActual("toma " + vaso.getNombre() + " del estante de vacíos");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
		return vaso;
	}
	
	public Vaso obtenerLleno() {
		Vaso vaso = null;
		try {
			this.lock.lock();
			while (this.cantLlenos == 0) {
				Padding.mostrarMsgDeThreadActual("no hay vasos llenos en el estante");
				this.cHayLlenos.await();
			}
			int num = this.findVaso(this.estanteLlenos);
			vaso = this.estanteLlenos[num];
			this.estanteLlenos[num] = null;
			this.cantLlenos--;
			Padding.mostrarMsgDeThreadActual("toma " + vaso.getNombre() + " del estante de llenos");
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
		return vaso;
	}
	
	public void llenar(Vaso vaso) {
		try {
			this.lock.lock();
			while (this.llenando) {
				Padding.mostrarMsgDeThreadActual("choppera en uso");
				this.cChopperaLibre.await();
			}
			this.llenando = true;
			Padding.mostrarMsgDeThreadActual("llena " + vaso.getNombre());
			this.llenando = false;
			this.cChopperaLibre.signalAll();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
	public void vaciar(Vaso vaso) {
		Padding.mostrarMsgDeThreadActual("vacía " + vaso.getNombre());
	}
	
	public void ponerLleno(Vaso vaso) {
		this.lock.lock();
		this.estanteLlenos[this.findLugar(this.estanteLlenos)] = vaso;
		this.cantLlenos++;
		Padding.mostrarMsgDeThreadActual("pone " + vaso.getNombre() + " en el estante de llenos");
		this.cHayLlenos.signalAll();
		this.lock.unlock();
	}
	
	public void ponerVacio(Vaso vaso) {
		this.lock.lock();
		this.estanteVacios[this.findLugar(this.estanteVacios)] = vaso;
		this.cantVacios++;
		Padding.mostrarMsgDeThreadActual("pone " + vaso.getNombre() + " en el estante de vacíos");
		this.cHayVacios.signalAll();
		this.lock.unlock();
	}
	
	private int findVaso(Vaso[] estante) {
		for (int i = 0; i < estante.length; i++) {
			if (estante[i] != null) {
				return i;
			}
		}
		return -1;
	}

	private int findLugar(Vaso[] estante) {
		for (int i = 0; i < estante.length; i++) {
			if (estante[i] == null) {
				return i;
			}
		}
		return -1;
	}

	public void mostrarEstadoFinal() {
		System.out.println("estante llenos: " + this.getVasosAsText(this.estanteLlenos));
		System.out.println("estante vacíos: " + this.getVasosAsText(this.estanteVacios));
	}
	
	private String getVasosAsText(Vaso[] estante) {
		return String.join(", ", Arrays.asList(estante)
										.stream()
										.filter(vaso -> vaso != null)
										.map(vaso -> vaso.getNombre())
										.collect(Collectors.toList()));
	}
	
}
