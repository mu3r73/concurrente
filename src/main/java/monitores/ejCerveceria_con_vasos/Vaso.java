package monitores.ejCerveceria_con_vasos;

public class Vaso {
	
	private String nombre;

	public Vaso(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return this.nombre;
	}

}
