package monitores.ejCerveceria_con_vasos;

import gen.Padding;

public class RCliente implements Runnable {
	
	private Cerveceria cmon;
	
	public RCliente(Cerveceria cmon) {
		super();
		this.cmon = cmon;
	}

	@Override
	public void run() {
		Padding.mostrarMsgDeThreadActual("quiere un vaso");
		Vaso vaso = this.cmon.obtenerLleno();
		this.cmon.vaciar(vaso);
		this.cmon.ponerVacio(vaso);
	}

}
