package monitores.ejCerveceria_con_vasos;

import java.util.Random;

import gen.Padding;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		Random rng = new Random();
		
		Cerveceria cmon = new Cerveceria(5);
		
		Thread[] tbs = new Thread[2];
		for (int i = 0; i < 2; i++) {
			tbs[i] = new Thread(new RBarman(cmon, 3), Padding.mkNombreConPadding("barman", i));
			tbs[i].start();
		}
		
		int cantClientes = 4 + rng.nextInt(3); // de 4 a 6 clientes
		Thread[] tcs = new Thread[cantClientes];
		for (int i = 0; i < tcs.length; i++) {
			tcs[i] = new Thread(new RCliente(cmon), Padding.mkNombreConPadding("cliente", i));
			tcs[i].start();
		}
		
		for (int i = 0; i < tbs.length; i++) {
			tbs[i].join();
		}
		for (int i = 0; i < tcs.length; i++) {
			tcs[i].join();
		}
		System.out.println("*** estado final ***");
		cmon.mostrarEstadoFinal();
	}
	
}
