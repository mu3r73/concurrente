package monitores.lectores_escritores_prior_escrit;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class BufferCompartido {
	
	private int cantLectoresEsperando;
	private int cantEscritoresEsperando;
	private int cantLeyendo;
	private boolean escribiendo;
	
	private Lock lock;
	private Condition cPuedeLeer;
	private Condition cPuedeEscribir;
	
	public BufferCompartido() {
		super();
		this.cantLectoresEsperando = 0;
		this.cantEscritoresEsperando = 0;
		this.cantLeyendo = 0;
		this.escribiendo = false;
		this.lock = new ReentrantLock();
		this.cPuedeLeer = lock.newCondition();
		this.cPuedeEscribir = lock.newCondition();
	}
	
	public void solicitarLectura() {
		this.lock.lock();
		this.cantLectoresEsperando++;
		Padding.mostrarMsgDeThreadActual("quiere leer");
		Padding.mostrarMsgDeThreadActual(this.cantLectoresEsperando + " lectores esperando");
		this.lock.unlock();
	}
	
	public void empezarLectura() {
		try {
			this.lock.lock();
			while (this.escribiendo || this.cantEscritoresEsperando != 0) {
				this.cPuedeLeer.await();
			}
			this.cantLectoresEsperando--;
			this.cantLeyendo++;
			Padding.mostrarMsgDeThreadActual("empezó a leer");
			Padding.mostrarMsgDeThreadActual(this.cantLectoresEsperando + " lectores esperando");
			this.cPuedeLeer.signal();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
	public void terminarLectura() {
		this.lock.lock();
		this.cantLeyendo--;
		Padding.mostrarMsgDeThreadActual("terminó de leer");
		if (cantLeyendo == 0) {
			this.cPuedeEscribir.signal();
		}
		this.lock.unlock();
	}
	
	public void solicitarEscritura() {
		this.lock.lock();
		this.cantEscritoresEsperando++;
		Padding.mostrarMsgDeThreadActual("quiere escribir");
		Padding.mostrarMsgDeThreadActual(this.cantEscritoresEsperando + " escritores esperando");
		this.lock.unlock();
	}
	
	public void empezarEscritura() {
		try {
			this.lock.lock();
			while (this.cantLeyendo != 0 || this.escribiendo) {
				this.cPuedeEscribir.await();
			}
			this.cantEscritoresEsperando--;
			this.escribiendo = true;
			Padding.mostrarMsgDeThreadActual("empezó a escribir");
			Padding.mostrarMsgDeThreadActual(this.cantEscritoresEsperando + " escritores esperando");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}
	
	public void terminarEscritura() {
		this.lock.lock();
		this.escribiendo = false;
		Padding.mostrarMsgDeThreadActual("terminó de escribir");
		if (this.cantLectoresEsperando > 0) {
			this.cPuedeLeer.signal();
		} else {
			this.cPuedeEscribir.signal();
		}
		this.lock.unlock();
	}
	
}
