package monitores.lectores_escritores_prior_escrit;

public class REscritor implements Runnable {

	private BufferCompartido bcmon;

	public REscritor(BufferCompartido bcmon) {
		super();
		this.bcmon = bcmon;
	}

	@Override
	public void run() {
		bcmon.solicitarEscritura();
		bcmon.empezarEscritura();
		bcmon.terminarEscritura();
	}

}
