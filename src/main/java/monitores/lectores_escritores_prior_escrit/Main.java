package monitores.lectores_escritores_prior_escrit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Random rng = new Random();
		
		BufferCompartido bcmon = new BufferCompartido();
		
		List<Thread> lts = new ArrayList<>();
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {
			lts.add(new Thread(new RLector(bcmon), Padding.mkNombreConPadding("lector", i)));
		}
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {
			lts.add(new Thread(new REscritor(bcmon), Padding.mkNombreConPadding("escritor", i)));
		}
		
		Collections.shuffle(lts);
		
		for (Thread t : lts) {
			t.start();
		}
	}
	
}
