package monitores.lectores_escritores_prior_lect;

public class RLector implements Runnable {

	private BufferCompartido bcmon;
	
	public RLector(BufferCompartido bcmon) {
		super();
		this.bcmon = bcmon;
	}

	@Override
	public void run() {
		this.bcmon.solicitarLectura();
		this.bcmon.empezarLectura();
		this.bcmon.terminarLectura();
	}

}
