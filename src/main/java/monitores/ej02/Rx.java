package monitores.ej02;

import gen.Padding;

public class Rx implements Runnable {
	
	private Monitor mon;
	private Dado dado;
	
	public Rx(Monitor mon, Dado dado) {
		super();
		this.mon = mon;
		this.dado = dado;
	}

	@Override
	public void run() {
		int num = this.dado.tirar();
		Padding.mostrarMsgDeThreadActual("tirada = " + num);
		this.mon.procesar(num);
		Padding.mostrarMsgDeThreadActual("liberado");
	}

}
