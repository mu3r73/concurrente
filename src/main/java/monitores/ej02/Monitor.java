package monitores.ej02;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class Monitor {
	
	private Lock lock = new ReentrantLock();
	private Condition cPares = lock.newCondition();
	private Condition cImpares = lock.newCondition();
	
	public void procesar(int num) {
		try {
			this.lock.lock();
			
			switch (num) {
				case 2:
				case 4:
				case 6:
				case 8:
					Padding.mostrarMsgDeThreadActual("bloqueado en grupo par");
					this.cPares.await();
					break;
				case 1:
				case 3:
				case 5:
				case 7:
					Padding.mostrarMsgDeThreadActual("bloqueado en grupo impar");
					this.cImpares.await();
					break;
				case 9:
					Padding.mostrarMsgDeThreadActual("liberando threads del grupo impar");
					this.cPares.signalAll();
					break;
				case 10:
					Padding.mostrarMsgDeThreadActual("liberando threads del grupo par");
					this.cImpares.signalAll();
					break;
				default:
					break;
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}

}
