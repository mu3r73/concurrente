package monitores.ej02;

import gen.Padding;

/*
 * generalmente no termina, salvo que se generen 9 y 10 al final
 */
public class Main {

	public static void main(String[] args) {
		Dado dado = new Dado(10);
		Monitor mon = new Monitor();
		
		for(int i = 0; i < 20; i++) {
			new Thread(new Rx(mon, dado), Padding.mkNombreConPadding("thread", i)).start();
		}
	}
	
}
