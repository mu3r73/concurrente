package monitores.ej02;

import java.util.Random;

public class Dado {
	
	private int caras;
	private Random rng = new Random();

	public Dado(int caras) {
		super();
		this.caras = caras;
	}
	
	public int tirar() {
		return 1 + this.rng.nextInt(this.caras); 
	}

}
