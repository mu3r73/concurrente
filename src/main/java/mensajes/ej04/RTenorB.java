package mensajes.ej04;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

public class RTenorB implements Runnable {

	private String name;
	private String queueName;
	private String msg;

	public RTenorB(String queueName, String msg) {
		super();
		this.name = "Tenor B";
		this.queueName = queueName;
		this.msg = msg;
	}

	@Override
	public void run() {
		try {
			// conexión
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			connectionFactory.setUseAsyncSend(true); // send no bloqueante
			Connection connection = connectionFactory.createConnection();
			connection.start();
			// sesión
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// cola
			Queue queue = session.createQueue(this.queueName);
			// consumidor
			MessageConsumer consumer = session.createConsumer(queue);
			// productor
			MessageProducer producer = session.createProducer(queue);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

			// esperar que termine Tenor A
			boolean puedeCantar = false;
			while (!puedeCantar) {
				TextMessage mA = (TextMessage) consumer.receive();
				String tA = mA.getText();
				puedeCantar = tA.equals("FIN_A");
				if (!puedeCantar) {
					// si no es el mensaje que esperaba, lo devuelvo a la cola
					producer.send(mA);
				}
			}
			
			// cantar
			Padding.mostrarMsg(this.name, this.msg);
			
			// enviar mensaje p/ avisar que ya puede cantar C
			Message m = session.createTextMessage("FIN_B");
			producer.send(m);
			
			// cerrar y desconectar
			session.close();
			connection.close();
			
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}
	
}
