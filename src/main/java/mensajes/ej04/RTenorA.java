package mensajes.ej04;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

public class RTenorA implements Runnable {

	private String name;
	private String queueName;
	private String msg;

	public RTenorA(String queueName, String msg) {
		super();
		this.name = "Tenor A";
		this.queueName = queueName;
		this.msg = msg;
	}

	@Override
	public void run() {
		try {
			// conexión
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			connectionFactory.setUseAsyncSend(true); // send no bloqueante
			Connection connection = connectionFactory.createConnection();
			connection.start();
			// sesión
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// cola
			Queue queue = session.createQueue(this.queueName);
			// productor
			MessageProducer producer = session.createProducer(queue);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

			// cantar
			Padding.mostrarMsg(this.name, this.msg);

			// enviar mensaje p/ avisar que ya puede cantar B
			Message m = session.createTextMessage("FIN_A");
			producer.send(m);

			// cerrar y desconectar
			session.close();
			connection.close();
			
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}

}
