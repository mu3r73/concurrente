package mensajes.ej04;

import gen.Padding;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		String qName = "ejercicio4";

		Padding.mostrarMsg("main", "Inicio Himno");

		Thread tA = new Thread(new RTenorA(qName, "Oíd mortales"), "Tenor A");
		Thread tB = new Thread(new RTenorB(qName, "el grito sagrado"), "Tenor B");
		Thread tC = new Thread(new RTenorC(qName, "Libertad, Libertad, Libertad !!!"), "Tenor C");
		tC.start();
		tB.start();
		tA.start();

		tA.join();
		tB.join();
		tC.join();

		Padding.mostrarMsg("main", "Fin Himno");
	}

}
