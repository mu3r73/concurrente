package mensajes.ej02;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

public class RProductor implements Runnable {

	private String queueName;
	private Item item;

	public RProductor(String queueName, Item item) {
		super();
		this.queueName = queueName;
		this.item = item;
	}

	@Override
	public void run() {
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			connectionFactory.setUseAsyncSend(true); // send no bloqueante
			Connection connection = connectionFactory.createConnection();
			connection.start();
	
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Queue queue = session.createQueue(this.queueName);

			MessageProducer producer = session.createProducer(queue);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			
			Padding.mostrarMsgDeThreadActual("enviando mensaje '" + this.item.toString() + "'");
			Message m = session.createObjectMessage(this.item);
			producer.send(m);
			
			session.close();
			connection.close();
		
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();	
		}
	}
	
}
