package mensajes.ej02;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

public class RConsumidorBloq implements Runnable {

	private String queueName;

	public RConsumidorBloq(String queueName) {
		super();
		this.queueName = queueName;
	}
	
	@Override
	public void run() {
		try {
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			Connection connection = connectionFactory.createConnection();
			connection.start();
	
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Queue queue = session.createQueue(this.queueName);
			
			MessageConsumer consumer = session.createConsumer(queue);
			
			Padding.mostrarMsgDeThreadActual("esperando mensaje...");
			ObjectMessage msg = (ObjectMessage) consumer.receive();
			Item item = (Item) msg.getObject();
			Padding.mostrarMsgDeThreadActual("recibió mensaje '" + item.toString() + "'");
			
			session.close();
			connection.close();
		
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}

}
