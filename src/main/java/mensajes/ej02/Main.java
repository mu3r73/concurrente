package mensajes.ej02;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		String qName = "ejercicio2";

		for (int i = 1; i < 9; i++) {
			new Thread(new RConsumidorBloq(qName), Padding.mkNombreConPadding("consumidor", i)).start();
		}

		for (int i = 1; i < 9; i++) {
			new Thread(new RProductor(qName, new Item("item" + i, i)), Padding.mkNombreConPadding("productor", i)).start();
		}
	}

}
