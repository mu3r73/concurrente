package mensajes.ej08;

import javax.jms.JMSException;

import gen.Padding;

public class Main {
	
	public static void main(String[] args) {
		try {
			// creo publishers con sus tópicos
			Publisher[] publishers = new Publisher[3];
			for (int i = 0; i < publishers.length; i++) {
				publishers[i] = new Publisher(Padding.mkNombreConPadding("publisher", i), "topic" + i);
			}
			
			// creo subscribers
			Subscriber[] subscribers = new Subscriber[3];
			for (int i = 0; i < subscribers.length; i++) {
				subscribers[i] = new Subscriber(Padding.mkNombreConPadding("subscriber", i));
			}
			// asigno tópicos a subscribers
			subscribers[0].subscribe("topic0");
			subscribers[0].subscribe("topic1");
			subscribers[1].subscribe("topic1");
			subscribers[1].subscribe("topic2");
			subscribers[2].subscribe("topic2");
			subscribers[2].subscribe("topic0");
			
			// publishers escriben
			for (int i = 0; i < publishers.length; i++) {
				publishers[i].write("mensaje" + i);
			}
			
			// subscribers leen
			for (int i = 0; i < subscribers.length; i++) {
				for (int j = 0; j < publishers.length; j++) {
					subscribers[i].read("topic" + j, 1000);
				}
			}
			
			// fin
			for (int i = 0; i < publishers.length; i++) {
				publishers[i].close();
			}
			for (int i = 0; i < subscribers.length; i++) {
				subscribers[i].close();
			}
			
		
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
