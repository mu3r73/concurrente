package mensajes.ej08;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

/*
 * puede suscribirse a N tópicos
 */
public class Subscriber {

	private String name;

	private Connection connection;
	private Session session;
	private Map<String, MessageConsumer> consumers;

	public Subscriber(String name) throws JMSException {
		super();
		this.name = name;
		this.init();
	}

	private void init() throws JMSException {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
		this.connection = connectionFactory.createConnection();
		this.connection.start();

		this.session = this.connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		this.consumers = new HashMap<>();
	}

	public void subscribe(String topicName) throws JMSException {
		Topic topic = this.session.createTopic(topicName);

		MessageConsumer consumer = session.createConsumer(topic);

		this.consumers.put(topicName, consumer);
	}

	public void read(String topicName, int timeout) throws JMSException {
		MessageConsumer consumer = this.consumers.getOrDefault(topicName, null);
		if (consumer == null) {
			Padding.mostrarMsg(this.name, "no está suscripto a [" + topicName + "]");
			return;
		}
		
		Padding.mostrarMsg(this.name, "esperando mensaje...");
		Message msg = consumer.receive(timeout);
		
		if (msg != null) {
			Padding.mostrarMsg(this.name, "recibió mensaje '[" + topicName + "] " + ((TextMessage) msg).getText() + "'");
			
		} else {
			Padding.mostrarMsg(this.name, "[" + topicName + "] no tiene mensajes");
		}
	}

	public void close() throws JMSException {
		this.session.close();
		this.connection.close();
	}

}
