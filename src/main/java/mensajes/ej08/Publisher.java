package mensajes.ej08;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

/*
 * publica 1 solo tópico
 */
public class Publisher {

	private String name;
	private String topicName;
	
	private Connection connection;
	private Session session;
	private MessageProducer producer;

	public Publisher(String name, String topicName) throws JMSException {
		super();
		this.name = name;
		this.topicName = topicName;
		this.init();
	}

	private void init() throws JMSException {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
		connectionFactory.setUseAsyncSend(true); // send no bloqueante
		this.connection = connectionFactory.createConnection();
		this.connection.setClientID(this.name);
		this.connection.start();

		this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Topic topic = this.session.createTopic(this.topicName);
		
		this.producer = this.session.createProducer(topic);
		this.producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
	}

	public void write(String txt) throws JMSException {
		TextMessage m = this.session.createTextMessage(txt);
		this.producer.send(m);
		Padding.mostrarMsg(this.name, "publicó el mensaje '[" + this.topicName + "] " + txt + "'");
	}
	
	public void close() throws JMSException {
		this.session.close();
		this.connection.close();
	}

}
