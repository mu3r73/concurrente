package mensajes.ej01;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

public class RProductor implements Runnable {

	private String queueName;
	private String msg;

	public RProductor(String queueName, String msg) {
		super();
		this.queueName = queueName;
		this.msg = msg;
	}
	
	@Override
	public void run() {
		try {
			// conexión
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			connectionFactory.setUseAsyncSend(true); // send no bloqueante
			Connection connection = connectionFactory.createConnection();
			connection.start();
			// sesión
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// cola
			Queue queue = session.createQueue(this.queueName);
			// productor
			MessageProducer producer = session.createProducer(queue);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

			// enviar mensaje
			Padding.mostrarMsgDeThreadActual("enviando mensaje '" + this.msg + "'");
			Message m = session.createTextMessage(this.msg);
			producer.send(m);

			// cerrar y desconectar
			session.close();
			connection.close();

		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}

}
