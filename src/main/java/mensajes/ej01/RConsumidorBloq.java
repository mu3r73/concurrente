package mensajes.ej01;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

public class RConsumidorBloq implements Runnable {

	private String queueName;

	public RConsumidorBloq(String queueName) {
		super();
		this.queueName = queueName;
	}

	@Override
	public void run() {
		try {
			// conexión
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			Connection connection = connectionFactory.createConnection();
			connection.start();
			// sesión
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// cola
			Queue queue = session.createQueue(this.queueName);
			// consumidor
			MessageConsumer consumer = session.createConsumer(queue);

			// recibir mensaje
			Message msg = consumer.receive();
			Padding.mostrarMsgDeThreadActual("esperando mensaje...");
			while (!(msg instanceof TextMessage)) {
				msg = consumer.receive();
			}
			Padding.mostrarMsgDeThreadActual("recibió mensaje '" + ((TextMessage) msg).getText() + "'");

			// cerrar y desconectar
			session.close();
			connection.close();
		
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}
	
}
