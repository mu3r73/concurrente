package mensajes.ej01;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		String qName = "ejercicio1";

		for (int i = 1; i < 9; i++) {
			new Thread(new RConsumidorBloq(qName), Padding.mkNombreConPadding("consumidor", i)).start();
		}

		for (int i = 1; i < 9; i++) {
			new Thread(new RProductor(qName, "mensaje" + i), Padding.mkNombreConPadding("productor", i)).start();
		}
	}

}
