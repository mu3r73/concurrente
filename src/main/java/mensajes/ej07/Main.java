package mensajes.ej07;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		String qName = "ejercicio7";

		for (int i = 1; i < 9; i++) {
			new Thread(new RConsumidorNoBloq(i, qName), Padding.mkNombreConPadding("consumidor", i)).start();
		}

		for (int i = 1; i < 9; i++) {
			new Thread(new RProductor(qName, "mensaje" + i), Padding.mkNombreConPadding("productor", i)).start();
		}
	}

}
