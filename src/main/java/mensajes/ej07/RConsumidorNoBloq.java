package mensajes.ej07;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

/*
 * consumidor no bloqueante
 * consume 1 solo mensaje
 * reemplaza latch por monitor
 */
public class RConsumidorNoBloq implements Runnable {

	private int num;
	private String queueName;
	private Monitor monitor;

	public RConsumidorNoBloq(int num, String queueName) {
		super();
		this.num = num;
		this.queueName = queueName;
		this.monitor = new Monitor(num);
	}

	@Override
	public void run() {
		try {
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			Connection connection = connectionFactory.createConnection();
			connection.start();
			
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Queue queue = session.createQueue(this.queueName);
			
			MessageConsumer consumer = session.createConsumer(queue);
			LeConsumerListener listener = new LeConsumerListener(this.num, this.monitor);
			consumer.setMessageListener(listener);
			
			this.monitor.esperarMensaje();
			Padding.mostrarMsgDeThreadActual("¡mensaje recibido!");

			session.close();
			connection.close();
		
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}
	
}
