package mensajes.ej07;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

/*
 * productor no bloqueante
 * produce 1 mensaje
 */
public class RProductor implements Runnable {

	private String queueName;
	private String msg;

	public RProductor(String queueName, String msg) {
		super();
		this.queueName = queueName;
		this.msg = msg;
	}
	
	@Override
	public void run() {
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			connectionFactory.setUseAsyncSend(true); // send no bloqueante
			Connection connection = connectionFactory.createConnection();
			connection.start();

			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Queue queue = session.createQueue(this.queueName);
			
			MessageProducer producer = session.createProducer(queue);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

			Padding.mostrarMsgDeThreadActual("enviando mensaje '" + this.msg + "'");
			Message m = session.createTextMessage(this.msg);
			producer.send(m);

			session.close();
			connection.close();

		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}

}
