package mensajes.ej07;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import gen.Padding;

public class LeConsumerListener implements MessageListener {

	private String name;
	private Monitor monitor;

	public LeConsumerListener(int num, Monitor monitor) {
		super();
		this.name = "listener" + num;
		this.monitor = monitor;
	}

	@Override
	public void onMessage(Message message) {
		try {
			TextMessage m = (TextMessage) message;
			Padding.mostrarMsg(this.name, "recibió '" + m.getText() + "'");
			this.monitor.recibirMensaje();
		
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
