package mensajes.ej07;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gen.Padding;

/*
 * monitor
 */
public class Monitor {

	private Lock lock = new ReentrantLock();
	private Condition cEsperandoMensaje = lock.newCondition();
	private String name;

	Monitor(int num) {
		this.name = "monitor" + num;
	}
	
	public void esperarMensaje() {
		try {
			this.lock.lock();
			Padding.mostrarMsg(this.name, "esperando mensaje...");
			this.cEsperandoMensaje.await();

		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}

	public void recibirMensaje() {
		this.lock.lock();
		Padding.mostrarMsg(this.name, "mensaje recibido");
		this.cEsperandoMensaje.signal();
		this.lock.unlock();
	}

}
