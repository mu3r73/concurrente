package mensajes.shared;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;

public class LeBroker {

	public final static String brokerURL = "tcp://localhost:61616";

	public static void main(String[] args) throws URISyntaxException, Exception {
		BrokerService broker = BrokerFactory.createBroker(new URI("broker:(" + LeBroker.brokerURL + ")"));
		broker.setDeleteAllMessagesOnStartup(true);
//		broker.setPersistent(true);
		broker.start();
	}

}
