package mensajes.ej03;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

public class RContador implements Runnable {
	// consumidor
	public static final String INC = "INC";
	public static final String FIN = "FIN";

	private String queueName;
	private int num;
	
	private int contador = 0;

	public RContador(String queueName, int num) {
		super();
		this.queueName = queueName;
		this.num = num;
	}

	@Override
	public void run() {
		try {
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			Connection connection = connectionFactory.createConnection();
			connection.start();
			
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
			Queue queue = session.createQueue(this.queueName);
			
			MessageConsumer consumer = session.createConsumer(queue);
			
			for (int i = 0; i < this.num; i++) {
				Padding.mostrarMsgDeThreadActual("esperando mensaje...");
				TextMessage msg = (TextMessage) consumer.receive();
				String txt = msg.getText();
				Padding.mostrarMsgDeThreadActual("recibió mensaje '" + txt + "'");
				if (txt.equals(RContador.INC)) {
					this.contador++;
				}
			}
			
			Padding.mostrarMsgDeThreadActual("resultado: " + this.contador);
			
			session.close();
			connection.close();
			
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}
	
}
