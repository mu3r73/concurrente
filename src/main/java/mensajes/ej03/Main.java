package mensajes.ej03;

import gen.Padding;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		String qName = "ejercicio3";
		
		Thread tsum1 = new Thread(new RSumador(qName, 5000), Padding.mkNombreConPadding("sumador", 1));
		tsum1.start();
		Thread tsum2 = new Thread(new RSumador(qName, 3000), Padding.mkNombreConPadding("sumador", 2));
		tsum2.start();
		
		Thread tcont = new Thread(new RContador(qName, 8000), Padding.mkNombreConPadding("contador", 0));
		tcont.start();
	}
	
}
