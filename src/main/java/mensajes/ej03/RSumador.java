package mensajes.ej03;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

public class RSumador implements Runnable {
	// productor

	private String queueName;
	private int num;

	public RSumador(String queueName, int num) {
		super();
		this.queueName = queueName;
		this.num = num;
	}
	
	@Override
	public void run() {
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			connectionFactory.setUseAsyncSend(true); // send no bloqueante
			Connection connection = connectionFactory.createConnection();
			connection.start();
	
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
			Queue queue = session.createQueue(this.queueName);
			
			MessageProducer producer = session.createProducer(queue);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			
			for (int i = 0; i < this.num; i++) {
				Padding.mostrarMsgDeThreadActual("enviando mensaje '" + RContador.INC + "'");
				Message msg = session.createTextMessage(RContador.INC);
				producer.send(msg);
			}
			
			session.close();
			connection.close();
			
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}

}
