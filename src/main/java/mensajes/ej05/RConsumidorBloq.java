package mensajes.ej05;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

/*
 * usa 2 colas activemq:
 * 1 para los mensajes propiamente dichos, y 1 para administrar espacio libre en el buffer 
 */
public class RConsumidorBloq implements Runnable {

	private String mainQName;

	public RConsumidorBloq(String mainQName) {
		super();
		this.mainQName = mainQName;
	}

	@Override
	public void run() {
		try {
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			Connection connection = connectionFactory.createConnection();
			connection.start();
	
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Queue mainQ = session.createQueue(this.mainQName);
			Queue bufferQ = session.createQueue(Buffer.NOMBRE);
			
			MessageConsumer consumer = session.createConsumer(mainQ);
			MessageProducer bufferProducer = session.createProducer(bufferQ);
			bufferProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			
			Padding.mostrarMsgDeThreadActual("esperando mensaje...");
			ObjectMessage msg = (ObjectMessage) consumer.receive();
			Item item = (Item) msg.getObject();
			Padding.mostrarMsgDeThreadActual("recibió mensaje '" + item.toString() + "'");
			
			TextMessage m = session.createTextMessage(Buffer.MSG_HAY_LUGAR);
			bufferProducer.send(m);
			
			session.close();
			connection.close();
		
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}
	
}
