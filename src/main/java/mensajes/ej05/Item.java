package mensajes.ej05;

import java.io.Serializable;

public class Item implements Serializable {
	
	private static final long serialVersionUID = -1438618356880560474L;

	private String descripcion;
	private int cantidad;
	
	public Item(String descripcion, int cantidad) {
		super();
		this.descripcion = descripcion;
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public int getCantidad() {
		return cantidad;
	}

	@Override
	public String toString() {
		return this.descripcion + " x " + this.cantidad;
	}
	
}
