package mensajes.ej05;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

public class Buffer {
	
	public static final String NOMBRE = "buffer";
	public static final String MSG_HAY_LUGAR = "hay lugar";

	private int size;

	public Buffer(int size) {
		super();
		this.size = size;
	}
	
	public void inicializar() throws JMSException {
		// conexión
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
		connectionFactory.setUseAsyncSend(true); // send no bloqueante
		Connection connection = connectionFactory.createConnection();
		connection.start();
		// sesión
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		try {
			// cola
			Queue bufferQ = session.createQueue(Buffer.NOMBRE);
			// productor
			MessageProducer bufferProducer = session.createProducer(bufferQ);
			// inicialmente hay lugar para this.size mensajes
			Padding.mostrarMsg("buffer", "agregando " + this.size + " lugares libres");
			for (int i = 0; i < this.size; i++) {
				TextMessage m = session.createTextMessage(Buffer.MSG_HAY_LUGAR);
				bufferProducer.send(m);
			}
		
		} finally {
			// cerrar sesión
			if (session != null) {
				session.close();
			}
			// cerrar conexión
			if (connection != null) {
				connection.close();
			}
		}

	}

}
