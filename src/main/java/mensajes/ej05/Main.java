package mensajes.ej05;

import javax.jms.JMSException;

import gen.Padding;

public class Main {

	public static void main(String[] args) throws JMSException {
		String qName = "ejercicio5";
		
		new Buffer(5).inicializar();
		
		for (int i = 1; i < 9; i++) {
			new Thread(new RProductor(qName, new Item("item" + i, i)), Padding.mkNombreConPadding("productor", i)).start();
		}
		for (int i = 1; i < 9; i++) {
			new Thread(new RConsumidorBloq(qName), Padding.mkNombreConPadding("consumidor", i)).start();
		}
	}

}
