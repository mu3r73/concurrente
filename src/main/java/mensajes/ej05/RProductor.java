package mensajes.ej05;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import gen.Padding;
import mensajes.shared.LeBroker;

/*
 * usa 2 colas activemq:
 * 1 para los mensajes propiamente dichos, y 1 para administrar espacio libre en el buffer 
 */
public class RProductor implements Runnable {

	private String mainQName;
	private Item item;

	public RProductor(String mainQName, Item item) {
		super();
		this.mainQName = mainQName;
		this.item = item;
	}

	@Override
	public void run() {
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
			connectionFactory.setUseAsyncSend(true); // send no bloqueante
			Connection connection = connectionFactory.createConnection();
			connection.start();
	
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Queue mainQ = session.createQueue(this.mainQName);
			Queue bufferQ = session.createQueue(Buffer.NOMBRE);

			MessageConsumer bufferConsumer = session.createConsumer(bufferQ);
			MessageProducer producer = session.createProducer(mainQ);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

			Padding.mostrarMsgDeThreadActual("esperando para enviar mensaje...");
			TextMessage msg = (TextMessage) bufferConsumer.receive();
			while (!msg.getText().equals(Buffer.MSG_HAY_LUGAR)) {
				msg = (TextMessage) bufferConsumer.receive();
			}
			
			Padding.mostrarMsgDeThreadActual("enviando mensaje '" + this.item.toString() + "'");
			Message m = session.createObjectMessage(this.item);
			producer.send(m);
			
			session.close();
			connection.close();
			
		} catch (Exception e) {
			System.out.println("caught: " + e);
			e.printStackTrace();
		}
	}
	
}
