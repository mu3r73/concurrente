package mensajes.ej06;

import javax.jms.JMSException;

import gen.Padding;

public class RLector implements Runnable {

	private BufferCompartido bc;

	public RLector(BufferCompartido bc) {
		super();
		this.bc = bc;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere leer");
			
			this.bc.adquirirMutEx();
			this.bc.cantleyendo++;
			if (this.bc.cantleyendo == 1) {
				this.bc.adquirirBloqueoDeEscritura();
			}
			this.bc.liberarMutEx();

			Padding.mostrarMsgDeThreadActual("leyendo");

			this.bc.adquirirMutEx();
			this.bc.cantleyendo--;
			if (this.bc.cantleyendo <= 0) {
				this.bc.liberarBloqueoDeEscritura();
			}
			this.bc.liberarMutEx();
		
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
