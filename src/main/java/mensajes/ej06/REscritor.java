package mensajes.ej06;

import javax.jms.JMSException;

import gen.Padding;

public class REscritor implements Runnable {

	private BufferCompartido bc;

	public REscritor(BufferCompartido bc) {
		super();
		this.bc = bc;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere escribir");
			this.bc.adquirirBloqueoDeEscritura();
			Padding.mostrarMsgDeThreadActual("escribiendo");
			this.bc.liberarBloqueoDeEscritura();

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
