package mensajes.ej06;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import mensajes.shared.LeBroker;

/*
 * implementa 1 mutex + 1 semáforo para bloqueo de escritura
 * mediante 2 colas activemq y mensajes
 */
public class BufferCompartido {
	
	private static final String MUTEX_LIBRE = "MUTEX_LIBRE";
	private static final String SE_PUEDE_ESCRIBIR = "SE_PUEDE_ESCRIBIR";

	public int cantleyendo = 0;
	
	private Connection connection;
	private Session session;

	private Queue mutExQ;
	private Queue bloqEscritQ;
	
	private MessageProducer mutExProducer;
	private MessageProducer bloqEscritProducer;
	private MessageConsumer mutExConsumer;
	private MessageConsumer bloqEscritConsumer;

	public BufferCompartido() throws JMSException {
		super();
		this.init();
		this.liberarMutEx();
		this.liberarBloqueoDeEscritura();
	}

	private void init() throws JMSException {
		// conexión
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(LeBroker.brokerURL);
		connectionFactory.setUseAsyncSend(true); // send no bloqueante
		this.connection = connectionFactory.createConnection();
		this.connection.start();
		// sesión
		this.session = this.connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		// colas
		this.mutExQ = session.createQueue("mutEx");
		this.bloqEscritQ = session.createQueue("bloqEscrit");
		
		// productores
		this.mutExProducer = session.createProducer(mutExQ);
		this.mutExProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		this.bloqEscritProducer = session.createProducer(bloqEscritQ);
		this.bloqEscritProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		
		// consumidores
		this.mutExConsumer = session.createConsumer(mutExQ);
		this.bloqEscritConsumer = session.createConsumer(bloqEscritQ);
	}

	public void liberarMutEx() throws JMSException {
		this.mutExProducer.send(this.session.createTextMessage(BufferCompartido.MUTEX_LIBRE));
	}
	
	public void adquirirMutEx() throws JMSException {
		TextMessage m = (TextMessage) this.mutExConsumer.receive();
		while (!m.getText().equals(BufferCompartido.MUTEX_LIBRE)) {
			m = (TextMessage) this.mutExConsumer.receive();
		}
	}

	public void liberarBloqueoDeEscritura() throws JMSException {
		this.bloqEscritProducer.send(this.session.createTextMessage(BufferCompartido.SE_PUEDE_ESCRIBIR));
	}

	public void adquirirBloqueoDeEscritura() throws JMSException {
		TextMessage m = (TextMessage) this.bloqEscritConsumer.receive();
		while (!m.getText().equals(BufferCompartido.SE_PUEDE_ESCRIBIR)) {
			m = (TextMessage) this.bloqEscritConsumer.receive();
		}
	}

	public void close() throws JMSException {
		this.session.close();
		this.connection.close();
	}

}
