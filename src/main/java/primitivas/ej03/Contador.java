package primitivas.ej03;

public class Contador {

	private int val = 0;
	
	public int getVal() {
		return this.val;
	}
	
	public synchronized void incContador() {
		this.val++;
	}
	
}
