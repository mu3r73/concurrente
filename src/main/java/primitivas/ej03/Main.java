package primitivas.ej03;

public class Main {
	
	public static void main(String[] args) {
		System.out.println("main: inicio");
		
		Contador c = new Contador();
		
		Thread t1 = new Thread(new RIncrementador(3000, c));
		t1.start();
		Thread t2 = new Thread(new RIncrementador(5000, c));
		t2.start();
		
		while (t1.isAlive() || t2.isAlive()) {
			System.out.println(c.getVal());
		}
		System.out.println(c.getVal());
		
		System.out.println("main: fin");
	}
	
}
