package primitivas.ej03;

public class RIncrementador implements Runnable {
	
	private int times;
	private Contador c;

	public RIncrementador(int times, Contador c) {
		super();
		this.times = times;
		this.c = c;
	}

	@Override
	public void run() {
		for (int i = 0; i < this.times; i++) {
			this.c.incContador();
		}
	}
	
}
