package primitivas.ej06;

import gen.Padding;

public class RBanderillero implements Runnable {

	private Auxiliar aux;
	
	public RBanderillero(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("hola");
			Thread.sleep(2500);
			Padding.mostrarMsgDeThreadActual("preparados... listos...");
			this.aux.darSenhal();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
