package primitivas.ej06;

import gen.Padding;

public class RCorredor implements Runnable {

	private Auxiliar aux;

	public RCorredor(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		try {
			this.aux.esperar();
			Padding.mostrarMsgDeThreadActual("empieza a correr");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
