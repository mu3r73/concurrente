package primitivas.ej06;

import java.util.Random;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Random rng = new Random();
		
		Auxiliar aux = new Auxiliar();
		
		new Thread(new RBanderillero(aux), Padding.mkNombreConPadding("banderillero", 0)).start();
		
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {	// entre 4 y 9 corredores
			new Thread(new RCorredor(aux), Padding.mkNombreConPadding("corredor", i + 1)).start();
		}
	}
	
}
