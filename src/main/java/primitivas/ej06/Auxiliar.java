package primitivas.ej06;

import gen.Padding;

public class Auxiliar {

	public synchronized void esperar() throws InterruptedException {
		Padding.mostrarMsgDeThreadActual("espera señal");
		this.wait();
	}
	
	public synchronized void darSenhal() {
		Padding.mostrarMsgDeThreadActual("emite señal de largada");
		this.notifyAll();
	}
	
}
