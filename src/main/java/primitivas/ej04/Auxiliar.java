package primitivas.ej04;

import gen.Padding;

public class Auxiliar {

	public synchronized void dormir() throws InterruptedException {
		Padding.mostrarMsgDeThreadActual("duerme");
		this.wait();
	}
	
	public synchronized void despertar() {
		Padding.mostrarMsgDeThreadActual("despierta");
		this.notify();
	}
	
}
