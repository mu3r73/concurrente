package primitivas.ej04;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Auxiliar aux = new Auxiliar();
		
		new Thread(new RDormilon(aux), Padding.mkNombreConPadding("dormilón", 0)).start();
		new Thread(new RHiperActivo(aux), Padding.mkNombreConPadding("hiperactivo", 0)).start();
	}
	
}
