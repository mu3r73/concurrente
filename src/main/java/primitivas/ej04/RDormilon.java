package primitivas.ej04;

import gen.Padding;

public class RDormilon implements Runnable {
	
	private Auxiliar aux;
	
	public RDormilon(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		try {
			aux.dormir();
			Padding.mostrarMsgDeThreadActual("se despierta");
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
