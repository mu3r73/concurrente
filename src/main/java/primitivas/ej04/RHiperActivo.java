package primitivas.ej04;

import gen.Padding;

public class RHiperActivo implements Runnable {

	private Auxiliar aux;
	
	public RHiperActivo(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("deja dormir");
			Thread.sleep(3000);
			aux.despertar();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
