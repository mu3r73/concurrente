package primitivas.ej05;

import gen.Padding;

public class Auxiliar {

	public synchronized void esperar() throws InterruptedException {
		Padding.mostrarMsgDeThreadActual("espera");
		this.wait();
	}
	
	public synchronized void atender() {
		Padding.mostrarMsgDeThreadActual("atiende");
		this.notify();
	}
	
}
