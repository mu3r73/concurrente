package primitivas.ej05;

import gen.Padding;

public class RPaciente implements Runnable {
	
	private Auxiliar aux;
	
	public RPaciente(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("entra a sala de espera");
			aux.esperar();
			Padding.mostrarMsgDeThreadActual("atendido ***por fin***");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
