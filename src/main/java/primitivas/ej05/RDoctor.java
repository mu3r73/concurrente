package primitivas.ej05;

import gen.Padding;

public class RDoctor implements Runnable {
	
	private Auxiliar aux;
	
	public RDoctor(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("hola");
			Thread.sleep(3000);
			Padding.mostrarMsgDeThreadActual("llama al siguiente paciente");
			aux.atender();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
