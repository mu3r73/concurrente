package primitivas.ej05;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Auxiliar aux = new Auxiliar();
		
		new Thread(new RDoctor(aux), Padding.mkNombreConPadding("doctor", 0)).start();
		
		for (int i = 0; i < 2; i++) {
			new Thread(new RPaciente(aux), Padding.mkNombreConPadding("paciente", i)).start();
		}
	}
	
}
