package primitivas.ej01;

public class R1 implements Runnable {
	
	private Auxiliar aux;
	
	public R1(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		this.aux.sync1("R1");
	}

}
