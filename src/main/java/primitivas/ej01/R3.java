package primitivas.ej01;

public class R3 implements Runnable {
	
	private Auxiliar aux;
	
	public R3(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		this.aux.nosync("R3");
	}

}
