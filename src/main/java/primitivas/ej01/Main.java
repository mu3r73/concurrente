package primitivas.ej01;

public class Main {

	public static void main(String[] args) {
		Auxiliar aux = new Auxiliar();
		
		System.out.println("dos procs synchronized del mismo objeto no se pueden ejecutar en // por distintos threads");
		new Thread(new R1(aux)).start();
		new Thread(new R2(aux)).start();
		System.out.println("... pero un método no sincrónico sí se puede ejecutar en //");
		new Thread(new R3(aux)).start();
	}
	
}
