package primitivas.ej01;

public class Auxiliar {
	
	public synchronized void sync1(String nombre) {
		try {
			System.out.println("sync1: iniciado por thread " + nombre);
			Thread.sleep(1500);
			System.out.println("sync1: fin");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public synchronized void sync2(String nombre) {
		try {
			System.out.println("sync2: iniciado por thread " + nombre);
			Thread.sleep(1500);
			System.out.println("sync2: fin");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void nosync(String nombre) {
		try {
			System.out.println("nosync: iniciado por thread " + nombre);
			Thread.sleep(1000);
			System.out.println("nosync: fin");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
