package primitivas.ej01;

public class R2 implements Runnable {
	
	private Auxiliar aux;
	
	public R2(Auxiliar aux) {
		super();
		this.aux = aux;
	}

	@Override
	public void run() {
		this.aux.sync2("R2");
	}

}
