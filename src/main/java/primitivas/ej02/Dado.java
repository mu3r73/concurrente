package primitivas.ej02;

import java.util.Random;

import gen.Padding;

public class Dado {
	
	private int caras;
	private Random rng = new Random();

	public Dado(int caras) {
		super();
		this.caras = caras;
	}
	
	public synchronized int tirar() {
		int num = 0;
		try {
			Padding.mostrarMsgDeThreadActual("tira el dado");
			Thread.sleep(1500);
			num = 1 + this.rng.nextInt(this.caras);
			Padding.mostrarMsgDeThreadActual("resultado de la tirada = " + num);
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return num; 
	}

}
