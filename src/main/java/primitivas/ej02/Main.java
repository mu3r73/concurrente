package primitivas.ej02;

import java.util.Random;

import gen.Padding;

public class Main {

	public static void main(String[] args) {
		Random rng = new Random();
		
		Dado d = new Dado(12);
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {
			new Thread(new RJugador(d), Padding.mkNombreConPadding("jugador", i)).start();
		}
	}
	
}
