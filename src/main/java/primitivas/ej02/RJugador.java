package primitivas.ej02;

public class RJugador implements Runnable {
	
	private Dado d;
	
	public RJugador(Dado d) {
		super();
		this.d = d;
	}

	@Override
	public void run() {
		d.tirar();
	}

}
