package semaforos.lectores_escritores_prior_escrit;

import java.util.concurrent.Semaphore;

public class BufferCompartido {
	
	public Semaphore sMutEx = new Semaphore(1);
	public Semaphore sEscrit = new Semaphore(1);
	public Semaphore sLect = new Semaphore(1);
	
	public boolean escribiendo = false;
	
	public int cantLeyendo = 0;
	public int cantLectorsEsperando = 0;
	public int cantEscritoresEsperando = 0;

}
