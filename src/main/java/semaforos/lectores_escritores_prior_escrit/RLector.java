package semaforos.lectores_escritores_prior_escrit;

import gen.Padding;

public class RLector implements Runnable {

	private BufferCompartido bc;
	
	public RLector(BufferCompartido bc) {
		super();
		this.bc = bc;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere leer");
			this.bc.sMutEx.acquire();
			
			if (this.bc.escribiendo || this.bc.cantEscritoresEsperando > 0) {
				this.bc.cantLectorsEsperando++;
				this.bc.sMutEx.release();
				this.bc.sLect.acquire();
				this.bc.sMutEx.acquire();
				this.bc.cantLectorsEsperando--;
			}
			this.bc.cantLeyendo++;
			if (this.bc.cantLectorsEsperando > 0) {
				this.bc.sLect.release();
			} else {
				this.bc.sMutEx.release();
			}
			
			Padding.mostrarMsgDeThreadActual("leyendo");
			
			this.bc.sMutEx.acquire();
			this.bc.cantLeyendo--;
			if (this.bc.cantLeyendo == 0 && this.bc.cantEscritoresEsperando > 0) {
				this.bc.sEscrit.release();
			} else {
				this.bc.sMutEx.release();
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
