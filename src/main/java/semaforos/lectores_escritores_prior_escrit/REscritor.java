package semaforos.lectores_escritores_prior_escrit;

import gen.Padding;

public class REscritor implements Runnable {
	
	private BufferCompartido bc;
	
	public REscritor(BufferCompartido bc) {
		super();
		this.bc = bc;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere escribir");
			
			this.bc.sMutEx.acquire();
			
			while (this.bc.cantLeyendo > 0 || this.bc.escribiendo) {
				this.bc.cantEscritoresEsperando++;
				this.bc.sMutEx.release();
				this.bc.sEscrit.acquire();
				this.bc.sMutEx.acquire();
				this.bc.cantEscritoresEsperando--;
			}
			
			this.bc.escribiendo = true;
			this.bc.sMutEx.release();
			
			Padding.mostrarMsgDeThreadActual("escribe");
			
			this.bc.sMutEx.acquire();
			this.bc.escribiendo = false;
			
			if (this.bc.cantEscritoresEsperando > 0) {
				this.bc.sEscrit.release();
			} else if (this.bc.cantLectorsEsperando > 0) {
				this.bc.sEscrit.release();
			} else {
				this.bc.sMutEx.release();
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
