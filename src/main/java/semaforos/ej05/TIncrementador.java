package semaforos.ej05;

import gen.Padding;

public class TIncrementador extends Thread {

	private int times;

	public TIncrementador(String nombre, int times) {
		super(nombre);
		this.times = times;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < this.times; i++) {
				ContadorCompartido.sMutEx.acquire();
				Padding.mostrarMsgDeThreadActual("contador++");
				ContadorCompartido.val++;
				ContadorCompartido.sMutEx.release();
			}
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
