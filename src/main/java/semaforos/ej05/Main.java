package semaforos.ej05;

import gen.Padding;

public class Main {
	
	public static void main(String[] args) {
		Thread tinc1 = new TIncrementador(Padding.mkNombreConPadding("incrementador", 0), 20);
		Thread tinc2 = new TIncrementador(Padding.mkNombreConPadding("incrementador", 1), 20);
		
		tinc1.start();
		tinc2.start();
		
		while (tinc1.isAlive() || tinc2.isAlive()) {
			continue;
		}
		
		System.out.println(ContadorCompartido.val);
	}

}
