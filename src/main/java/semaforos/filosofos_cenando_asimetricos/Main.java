package semaforos.filosofos_cenando_asimetricos;

import gen.Padding;

/*
 * ojo: si los filósofos corren en loop infinito, nunca termina
 */
public class Main {
	
	public static Runnable generarFilosofo(int num, Compartidos recs) {
		if (num % 2 == 0) {
			return new RFilosofoPar(num, recs);
		} else {
			return new RFilosofoImpar(num, recs);
		}
	}
	
	public static void main(String[] args) {
		Compartidos recs = new Compartidos();
		
		for (int i = 0; i < 5; i++) {
			new Thread(generarFilosofo(i, recs), Padding.mkNombreConPadding("filósofo", i)).start();
		}
	}

}
