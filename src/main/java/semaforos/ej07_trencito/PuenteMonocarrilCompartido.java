package semaforos.ej07_trencito;

import java.util.concurrent.Semaphore;

public class PuenteMonocarrilCompartido {
	
	public Semaphore sMutEx = new Semaphore(1);
	public Semaphore sMano = new Semaphore(1);
	public Semaphore sContramano = new Semaphore(1);
	
	public int cantCruzandoDirMano = 0;
	public int cantCruzandoDirContramano = 0;
	
}
