package semaforos.ej07_trencito;

import gen.Padding;

public class RAutoContramano implements Runnable {
	
	private String dir;
	private PuenteMonocarrilCompartido puente;
	
	public RAutoContramano(PuenteMonocarrilCompartido puente) {
		super();
		this.dir = "contramano";
		this.puente = puente;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere cruzar puente en dirección " + this.dir);
			
			if (this.puente.cantCruzandoDirContramano <= 0) {
				this.puente.sMano.acquire();
				this.puente.sContramano.acquire();
			}
			
			this.puente.sMutEx.acquire();
			Padding.mostrarMsgDeThreadActual("empieza a cruzar en dirección " + this.dir);
			this.puente.cantCruzandoDirContramano++;
			this.puente.sMutEx.release();
			
			Thread.sleep(1500);
			
			this.puente.sMutEx.acquire();
			this.puente.cantCruzandoDirContramano--;
			Padding.mostrarMsgDeThreadActual("termina de cruzar");
			this.puente.sMutEx.release();
			
			if (this.puente.cantCruzandoDirContramano <= 0) {
				this.puente.sContramano.release();
				this.puente.sMano.release();
			}
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
