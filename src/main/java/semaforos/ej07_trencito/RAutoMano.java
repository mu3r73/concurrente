package semaforos.ej07_trencito;

import gen.Padding;

public class RAutoMano implements Runnable {
	
	private String dir;
	private PuenteMonocarrilCompartido puente;
	
	public RAutoMano(PuenteMonocarrilCompartido puente) {
		super();
		this.dir = "mano";
		this.puente = puente;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere cruzar puente en dirección " + this.dir);
			
			if (this.puente.cantCruzandoDirMano <= 0) {
				this.puente.sContramano.acquire();
				this.puente.sMano.acquire();
			}
			
			this.puente.sMutEx.acquire();
			Padding.mostrarMsgDeThreadActual("empieza a cruzar en dirección " + this.dir);
			this.puente.cantCruzandoDirMano++;
			this.puente.sMutEx.release();
			
			Thread.sleep(1500);
			
			this.puente.sMutEx.acquire();
			this.puente.cantCruzandoDirMano--;
			Padding.mostrarMsgDeThreadActual("termina de cruzar");
			this.puente.sMutEx.release();
			
			if (this.puente.cantCruzandoDirMano <= 0) {
				this.puente.sMano.release();
				this.puente.sContramano.release();
			}
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
