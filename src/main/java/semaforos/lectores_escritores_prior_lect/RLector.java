package semaforos.lectores_escritores_prior_lect;

import gen.Padding;

public class RLector implements Runnable {

	private BufferCompartido bc;
	
	public RLector(BufferCompartido bc) {
		super();
		this.bc = bc;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere leer");
			
			this.bc.sMutEx.acquire();
			this.bc.cantleyendo++;
			if (this.bc.cantleyendo == 1) {
				this.bc.sBloqEscrit.acquire();
			}
			this.bc.sMutEx.release();
			
			Padding.mostrarMsgDeThreadActual("leyendo");
			
			this.bc.sMutEx.acquire();
			this.bc.cantleyendo--;
			if (this.bc.cantleyendo <= 0) {
				this.bc.sBloqEscrit.release();
			}
			this.bc.sMutEx.release();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
