package semaforos.lectores_escritores_prior_lect;

import gen.Padding;

public class REscritor implements Runnable {

	private BufferCompartido bc;
	
	public REscritor(BufferCompartido bc) {
		super();
		this.bc = bc;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere escribir");
			
			this.bc.sBloqEscrit.acquire();
			
			Padding.mostrarMsgDeThreadActual("escribiendo");
			
			this.bc.sBloqEscrit.release();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
