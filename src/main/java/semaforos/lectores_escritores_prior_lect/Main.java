package semaforos.lectores_escritores_prior_lect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import gen.Padding;

public class Main {
	
	public static void main(String[] args) {
		Random rng = new Random();
		
		BufferCompartido bc = new BufferCompartido();
		
		// de 4 a 9 escritores
		int cantEscrit = 4 + rng.nextInt(6);
		// de 4 a 9 lectores
		int cantLect = 4 + rng.nextInt(6);
		
		List<Thread> lts = new ArrayList<>();
		for (int i = 0; i < cantEscrit; i++) {
			lts.add(new Thread(new REscritor(bc), Padding.mkNombreConPadding("escritor", i)));
		}
		for (int i = 0; i < cantLect; i++) {
			lts.add(new Thread(new RLector(bc), Padding.mkNombreConPadding("lector", i)));
		}
		Collections.shuffle(lts);
		
		for (Thread t : lts) {
			t.start();
		}
	}
	
}
