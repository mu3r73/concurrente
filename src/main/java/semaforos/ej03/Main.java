package semaforos.ej03;

import java.util.concurrent.Semaphore;

public class Main {
	
	public static void main(String[] args) {
		
		Semaphore sA = new Semaphore(0);
		Semaphore sB = new Semaphore(0);
		
		new Thread(new RA(sA, sB)).start();
		new Thread(new RB(sA, sB)).start();
		
	}

}
