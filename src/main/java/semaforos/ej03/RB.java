package semaforos.ej03;

import java.util.concurrent.Semaphore;

public class RB implements Runnable {
	
	private Semaphore sA;
	private Semaphore sB;
	
	public RB(Semaphore sA, Semaphore sB) {
		super();
		this.sA = sA;
		this.sB = sB;
	}

	@Override
	public void run() {
		try {
			System.out.println("c");
			sB.release();
			sA.acquire();
			System.out.println("d");
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
