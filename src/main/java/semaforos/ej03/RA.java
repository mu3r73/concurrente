package semaforos.ej03;

import java.util.concurrent.Semaphore;

public class RA implements Runnable {

	private Semaphore sA;
	private Semaphore sB;
	
	public RA(Semaphore sA, Semaphore sB) {
		super();
		this.sA = sA;
		this.sB = sB;
	}

	@Override
	public void run() {
		try {
			System.out.println("a");
			sA.release();
			sB.acquire();
			System.out.println("b");
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
