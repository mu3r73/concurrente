package semaforos.ej08;

import gen.Padding;

public class RAutoNorteSur implements Runnable {
	
	private PuenteMonocarrilCompartido puente1;
	private PuenteMonocarrilCompartido puente2;
	
	public RAutoNorteSur(PuenteMonocarrilCompartido puente1, PuenteMonocarrilCompartido puente2) {
		super();
		this.puente1 = puente1;
		this.puente2 = puente2;
	}

	@Override
	public void run() {
		this.cruzarPuente(this.puente1);
		this.cruzarPuente(this.puente2);
	}

	private void cruzarPuente(PuenteMonocarrilCompartido puente) {
		try {
			Padding.mostrarMsgDeThreadActual("quiere cruzar " + puente.nombre + " desde el norte");
			
			puente.sPuente.acquire();
			
			Padding.mostrarMsgDeThreadActual("empieza a cruzar " + puente.nombre + " desde el norte");
			
			Thread.sleep(1500);
			
			Padding.mostrarMsgDeThreadActual("termina de cruzar " + puente.nombre);
			
			puente.sPuente.release();
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
