package semaforos.ej08;

import java.util.concurrent.Semaphore;

public class PuenteMonocarrilCompartido {
	
	public String nombre;
	public Semaphore sPuente;
	
	public PuenteMonocarrilCompartido(String nombre) {
		super();
		this.nombre = nombre;
		this.sPuente = new Semaphore(1);
	}
	
}
