package semaforos.ej02;

import java.util.concurrent.Semaphore;

public class RVigilado implements Runnable {

	private Semaphore s;
	
	public RVigilado(Semaphore s) {
		super();
		this.s = s;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < 100000; i++) {
				// nada
			}		
			this.s.acquire();
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
