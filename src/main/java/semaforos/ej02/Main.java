package semaforos.ej02;

import java.util.concurrent.Semaphore;

public class Main {
	
	public static void main(String[] args) {
		Semaphore s = new Semaphore(0);
		
		Thread tVigilado = new Thread(new RVigilado(s));
		
		new Thread(new RVigilante(tVigilado)).start();
	}

}
