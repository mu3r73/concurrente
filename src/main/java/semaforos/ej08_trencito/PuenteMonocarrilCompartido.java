package semaforos.ej08_trencito;

import java.util.concurrent.Semaphore;

public class PuenteMonocarrilCompartido {
	
	public String nombre;
	
	public Semaphore sMutEx;
	public Semaphore sNorte;
	public Semaphore sSur;

	public int cantCruzandoDesdeElNorte;
	public int cantCruzandoDesdeElSur;

	public PuenteMonocarrilCompartido(String nombre) {
		super();
		this.nombre = nombre;
		this.sMutEx = new Semaphore(1);
		this.sNorte = new Semaphore(1);
		this.sSur = new Semaphore(1);
		this.cantCruzandoDesdeElNorte = 0;
		this.cantCruzandoDesdeElSur = 0;
	}
	
}
