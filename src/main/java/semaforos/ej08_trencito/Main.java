package semaforos.ej08_trencito;

import java.util.Random;

import gen.Padding;

/*
 * todos los autos avanzan a la misma velocidad
 * variante "trencito" de autos:
 * mientras un auto está cruzando desde una dirección,
 * permite que otros también crucen en la misma dirección
 */
public class Main {
	
	private static Random rng = new Random();
	
	private static Runnable generarAuto(PuenteMonocarrilCompartido puenteN, PuenteMonocarrilCompartido puenteS) {
		if (rng.nextBoolean()) {
			return new RAutoNorteSur(puenteN, puenteS);
		} else {
			return new RAutoSurNorte(puenteS, puenteN);
		}
	}
	
	public static void main(String[] args) {
		PuenteMonocarrilCompartido puenteN = new PuenteMonocarrilCompartido("puenteN");
		PuenteMonocarrilCompartido puenteS = new PuenteMonocarrilCompartido("puenteS");
		
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {	// entre 4 y 9 autos
			new Thread(generarAuto(puenteN, puenteS), Padding.mkNombreConPadding("auto", i)).start();
		}		
	}
	
}
