package semaforos.ej08_trencito;

import gen.Padding;

public class RAutoNorteSur implements Runnable {
	
	private String dirEntrada;
	private PuenteMonocarrilCompartido puente1;
	private PuenteMonocarrilCompartido puente2;
	
	public RAutoNorteSur(PuenteMonocarrilCompartido puente1, PuenteMonocarrilCompartido puente2) {
		super();
		this.dirEntrada = "norte";
		this.puente1 = puente1;
		this.puente2 = puente2;
	}

	@Override
	public void run() {
		this.cruzarPuente(this.puente1);
		this.cruzarPuente(this.puente2);
	}

	private void cruzarPuente(PuenteMonocarrilCompartido puente) {
		try {
			Padding.mostrarMsgDeThreadActual("quiere cruzar " + puente.nombre + " desde el " + this.dirEntrada);
			
			if (puente.cantCruzandoDesdeElNorte <= 0) {
				puente.sSur.acquire();
				puente.sNorte.acquire();
			}
			
			puente.sMutEx.acquire();
			Padding.mostrarMsgDeThreadActual("empieza a cruzar " + puente.nombre + " desde el " + this.dirEntrada);
			puente.cantCruzandoDesdeElNorte++;
			puente.sMutEx.release();
			
			Thread.sleep(1500);
			
			puente.sMutEx.acquire();
			puente.cantCruzandoDesdeElNorte--;
			Padding.mostrarMsgDeThreadActual("termina de cruzar " + puente.nombre);
			puente.sMutEx.release();
			
			if (puente.cantCruzandoDesdeElNorte <= 0) {
				puente.sNorte.release();
				puente.sSur.release();
			}
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
