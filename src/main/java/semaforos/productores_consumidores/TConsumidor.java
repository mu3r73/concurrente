package semaforos.productores_consumidores;

import gen.Padding;

public class TConsumidor implements Runnable {
	
	private BufferCompartido bc;
	
	public TConsumidor(BufferCompartido bc) {
		super();
		this.bc = bc;
	}

	@Override
	public void run() {
		try {
			this.bc.sLlenos.acquire();

			this.bc.sMutEx.acquire();
			String item = bc.retirarItem();
			this.bc.sMutEx.release();
			
			this.bc.sVacios.release();
			
			this.consumirItem(item);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void consumirItem(String item) {
		Padding.mostrarMsgDeThreadActual("consume " + item);
	}

}
