package semaforos.productores_consumidores;

import java.util.concurrent.Semaphore;

public class BufferCompartido {
	
	public Semaphore sMutEx;
	public Semaphore sLlenos;
	public Semaphore sVacios;
	
	private String[] buffer;
	private int posEncolar;
	private int posDesencolar;

	public BufferCompartido(int tamanho) {
		super();
		this.sMutEx = new Semaphore(1);
		this.sLlenos = new Semaphore(0);
		this.sVacios = new Semaphore(tamanho);
		
		this.buffer = new String[tamanho];
		this.posEncolar = 0;
		this.posDesencolar = 0;
	}
	
	public int getSize() {
		return this.buffer.length;
	}

	public void agregarItem(String item) {
		this.buffer[posEncolar] = item;
//		System.out.println("buffer compartido: agregado " + item + " en posición " + this.posEncolar);
		this.posEncolar = (this.posEncolar + 1) % this.getSize();
	}

	public String retirarItem() {
		String item = this.buffer[this.posDesencolar];
//		System.out.println("buffer compartido: retirando " + item + " de posición " + this.posDesencolar);
		this.buffer[this.posDesencolar] = null;
		this.posDesencolar = (this.posDesencolar + 1) % this.getSize();
		return item;
	}

}
