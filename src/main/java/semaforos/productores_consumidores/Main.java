package semaforos.productores_consumidores;

import java.util.Random;

import gen.Padding;

public class Main {
	public static void main(String[] args) {
		final int tamBuffer = 5;
		BufferCompartido bc = new BufferCompartido(tamBuffer);
		
		Random rng = new Random();
		
		// entre 4 y 9 productores
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {
			new Thread(new TProductor(bc), Padding.mkNombreConPadding("productor", i + 1)).start();
		}
		// entre 4 y 9 consumidores
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {
			new Thread(new TConsumidor(bc), Padding.mkNombreConPadding("consumidor", i + 1)).start();
		}
	}

}
