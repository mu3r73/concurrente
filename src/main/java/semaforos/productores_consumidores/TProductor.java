package semaforos.productores_consumidores;

import gen.Padding;

public class TProductor implements Runnable {
	
	private BufferCompartido bc;
	static private int cantFabricados = 0;
	
	public TProductor(BufferCompartido bc) {
		super();
		this.bc = bc;
	}

	@Override
	public void run() {
		try {
			String item = this.producirItem();
		
			this.bc.sVacios.acquire();

			this.bc.sMutEx.acquire();
			this.bc.agregarItem(item);
			this.bc.sMutEx.release();

			this.bc.sLlenos.release();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private String producirItem() {
		TProductor.cantFabricados++;
		String item = "item" + TProductor.cantFabricados;
		Padding.mostrarMsgDeThreadActual("produce " + item);
		return item;
	}
	
}
