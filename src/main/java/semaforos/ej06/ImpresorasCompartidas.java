package semaforos.ej06;

import java.util.concurrent.Semaphore;

public class ImpresorasCompartidas {

	public Semaphore sMutEx;
	public Semaphore sImpresoras;

	private boolean[] libres;

	public ImpresorasCompartidas(int cant) {
		super();
		this.sMutEx = new Semaphore(1);
		this.sImpresoras = new Semaphore(cant);
		this.inicializarImpresoras(cant);
	}

	private void inicializarImpresoras(int cant) {
		this.libres = new boolean[cant];
		for (int i = 0; i < cant; i++) {
			this.libres[i] = true;
		}
	}

	public int ocupar() {
		int pos = this.findLibre();
		this.libres[pos] = false;
		return pos + 1;
	}

	private int findLibre() {
		for (int i = 0; i < libres.length; i++) {
			if (libres[i]) {
				return i;
			}
		}
		return -1;
	}

	public void liberar(int pos) {
		this.libres[pos - 1] = true;
	}

}
