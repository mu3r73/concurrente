package semaforos.ej06;

import gen.Padding;

public class RProceso implements Runnable {
	
	private ImpresorasCompartidas ic;
	
	public RProceso(ImpresorasCompartidas ic) {
		super();
		this.ic = ic;
	}

	@Override
	public void run() {
		try {
			ic.sImpresoras.acquire();
			
			ic.sMutEx.acquire();
			int inum = ic.ocupar();
			ic.sMutEx.release();
			
			Padding.mostrarMsgDeThreadActual("imprimiendo en impresora" + inum);
			Thread.sleep(2000);

			ic.sMutEx.acquire();
			ic.liberar(inum);
			Padding.mostrarMsgDeThreadActual("liberando impresora" + inum);
			ic.sMutEx.release();

			ic.sImpresoras.release();
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
