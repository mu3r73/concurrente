package semaforos.ej06;

import gen.Padding;

public class Main {
		
	public static void main(String[] args) {
		int cantImpresoras = 3;
		int cantProcesos = 5;

		ImpresorasCompartidas ic = new ImpresorasCompartidas(cantImpresoras);
		
		for (int i = 0; i < cantProcesos; i++) {
			new Thread(new RProceso(ic), Padding.mkNombreConPadding("proceso", i)).start();
		}
	}

}
