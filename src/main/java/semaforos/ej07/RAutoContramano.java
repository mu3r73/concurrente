package semaforos.ej07;

import gen.Padding;

public class RAutoContramano implements Runnable {
	
	private String dir;
	private PuenteMonocarrilCompartido puente;
	
	public RAutoContramano(PuenteMonocarrilCompartido puente) {
		super();
		this.dir = "contramano";
		this.puente = puente;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere cruzar puente en dirección " + this.dir);
			
			this.puente.sPuente.acquire();
			
			Padding.mostrarMsgDeThreadActual("empieza a cruzar en dirección " + this.dir);
			
			Thread.sleep(1500);
			
			Padding.mostrarMsgDeThreadActual("termina de cruzar");
			
			this.puente.sPuente.release();
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
