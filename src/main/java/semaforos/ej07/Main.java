package semaforos.ej07;

import java.util.Random;

import gen.Padding;

/*
 * todos los autos avanzan a la misma velocidad
 */
public class Main {
	
	private static Random rng = new Random();

	private static Runnable generarAuto(PuenteMonocarrilCompartido puente) {
		if (rng.nextBoolean()) {
			return new RAutoMano(puente);
		} else {
			return new RAutoContramano(puente);
		}
	}
	
	public static void main(String[] args) {
		PuenteMonocarrilCompartido puente = new PuenteMonocarrilCompartido();
		
		for (int i = 0; i < 4 + rng.nextInt(6); i++) {	// entre 4 y 9 autos
			new Thread(generarAuto(puente), Padding.mkNombreConPadding("auto", i)).start();
		}
	}

}
