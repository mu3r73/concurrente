package semaforos.ej09;

import java.util.Random;

import gen.Padding;

/*
 * todos los salvajes comen a la misma velocidad
 */
public class Main {
	
	public static void main(String[] args) {
		try {
			Random rng = new Random();
			
			OllaCompartida oc = new OllaCompartida(3);
			
			// 1 cocinero
			Thread tc = new Thread(new RCocinero(oc), "cocinero");
			tc.start();
			
			// entre 4 y 9 salvajes
			Thread[] tss = new Thread[4 + rng.nextInt(6)];
			for (int i = 0; i < tss.length; i++) {
				tss[i] = new Thread(new RSalvaje(oc), Padding.mkNombreConPadding("salvaje", i + 1));
				tss[i].start();
			}
			
			for (int i = 0; i < tss.length; i++) {
				tss[i].join();
			}
			System.out.println("***fin***");

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
