package semaforos.ej09;

import gen.Padding;

public class RCocinero implements Runnable {

	private OllaCompartida oc;
	
	public RCocinero(OllaCompartida oc) {
		super();
		this.oc = oc;
	}

	@Override
	public void run() {
		try {
			while(true) {
				this.oc.sCocinar.acquire();
				
				this.oc.sOllaLibre.acquire();
				Padding.mostrarMsgDeThreadActual("va a cocinar");
				
				this.oc.sMutEx.acquire();
				this.oc.cocineroEnCamino = false;
				
				this.oc.cocinando = true;
				Padding.mostrarMsgDeThreadActual("empieza a cocinar");

				Thread.sleep(3000);
				this.oc.cantChuletas = this.oc.maxChuletas;
				this.oc.cocinando = false;
				
				this.oc.sMutEx.release();
				
				Padding.mostrarMsgDeThreadActual("termina de cocinar");
				Padding.mostrarMsgDeThreadActual("(quedan " + this.oc.cantChuletas + " chuletas)");
				this.oc.sOllaLibre.release();
			}
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
