package semaforos.ej09;

import gen.Padding;

public class RSalvaje implements Runnable {
	
	private OllaCompartida oc;
	
	public RSalvaje(OllaCompartida oc) {
		super();
		this.oc = oc;
	}

	@Override
	public void run() {
		try {
			Padding.mostrarMsgDeThreadActual("quiere comer");
			this.oc.sOllaLibre.acquire();
			Padding.mostrarMsgDeThreadActual("va a la olla");

			while (this.oc.cantChuletas <= 0 && !this.oc.cocinando) {
				Padding.mostrarMsgDeThreadActual("no hay chuletas");
				if (!this.oc.cocineroEnCamino) {
					this.oc.sMutEx.acquire();
					this.oc.cocineroEnCamino = true;
					this.oc.sMutEx.release();
					this.oc.sCocinar.release();
					Padding.mostrarMsgDeThreadActual("llama al cocinero");
				} else {
					Padding.mostrarMsgDeThreadActual("alguien ya llamó al cocinero");
				}
				Padding.mostrarMsgDeThreadActual("deja la olla");
				this.oc.sOllaLibre.release();

				Thread.sleep(1000);
				this.oc.sOllaLibre.acquire();
				Padding.mostrarMsgDeThreadActual("va a la olla");
			}

			this.oc.sMutEx.acquire();
			Padding.mostrarMsgDeThreadActual("toma 1 chuleta");
			this.oc.cantChuletas--;
			Padding.mostrarMsgDeThreadActual("(quedan " + this.oc.cantChuletas + " chuletas)");
			this.oc.sMutEx.release();
			
			Padding.mostrarMsgDeThreadActual("deja la olla");
			this.oc.sOllaLibre.release();
			
			Padding.mostrarMsgDeThreadActual("empieza a comer");
			Thread.sleep(1500);
			Padding.mostrarMsgDeThreadActual("termina de comer");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
