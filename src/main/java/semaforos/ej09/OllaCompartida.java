package semaforos.ej09;

import java.util.concurrent.Semaphore;

public class OllaCompartida {
	
	public Semaphore sMutEx = new Semaphore(1);
	public Semaphore sOllaLibre = new Semaphore(1);
	public Semaphore sCocinar = new Semaphore(1);
	
	public int maxChuletas;
	public int cantChuletas = 0;
	
	public boolean cocineroEnCamino = false;
	public boolean cocinando = false;
	
	public OllaCompartida(int maxChuletas) {
		super();
		this.maxChuletas = maxChuletas;
	}

}
