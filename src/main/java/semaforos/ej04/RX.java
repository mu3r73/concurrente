package semaforos.ej04;

import java.util.concurrent.Semaphore;

import gen.Padding;

public class RX implements Runnable {

	private Semaphore sX;
	private Semaphore sY;
	
	public RX(Semaphore sX, Semaphore sY) {
		super();
		this.sX = sX;
		this.sY = sY;
	}

	@Override
	public void run() {
		try {
			sY.acquire();
			Padding.mostrarMsgDeThreadActual("x");
			sX.release();
			Padding.mostrarMsgDeThreadActual("y");
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
