package semaforos.ej04;

import java.util.concurrent.Semaphore;

import gen.Padding;

/*
 * genera deadlock entre los 3 threads
 * (no muestra ningún mensaje)
 */
public class Main {
	
	public static void main(String[] args) {
		Semaphore s1 = new Semaphore(0);
		Semaphore s2 = new Semaphore(0);
		Semaphore s3 = new Semaphore(0);
		
		new Thread(new RX(s1, s3), Padding.mkNombreConPadding("t", 0)).start();
		new Thread(new RX(s2, s1), Padding.mkNombreConPadding("t", 1)).start();
		new Thread(new RX(s3, s2), Padding.mkNombreConPadding("t", 2)).start();
	}

}
