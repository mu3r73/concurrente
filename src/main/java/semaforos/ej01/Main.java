package semaforos.ej01;

import java.util.concurrent.Semaphore;

public class Main {
	
	public static void main(String[] args) {
		Semaphore s = new Semaphore(0);
		
		new Thread(new RA(s)).start();;
		new Thread(new RB(s)).start();
	}

}
