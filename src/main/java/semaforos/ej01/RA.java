package semaforos.ej01;

import java.util.concurrent.Semaphore;

public class RA implements Runnable {

	private Semaphore s;
	
	public RA(Semaphore s) {
		super();
		this.s = s;
	}

	@Override
	public void run() {
		System.out.println("a");
		s.release();
		System.out.println("b");
	}

}
