package semaforos.ej01;

import java.util.concurrent.Semaphore;

public class RB implements Runnable {
	
	private Semaphore s;
	
	public RB(Semaphore s) {
		super();
		this.s = s;
	}

	@Override
	public void run() {
		try {
			System.out.println("c");
			s.acquire();
			System.out.println("d");
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
