package semaforos.filosofos_cenando_permit_4;

import java.util.concurrent.Semaphore;

import gen.Padding;

public class RFilosofo implements Runnable {
	
	private int num;
	private Compartidos recs;

	public RFilosofo(int num, Compartidos recs) {
		super();
		this.num = num;
		this.recs = recs;
	}
	
	@Override
	public void run() {
		try {
			while (true) {
				Padding.mostrarMsgDeThreadActual("piensa");
				Thread.sleep(2000);
				Padding.mostrarMsgDeThreadActual("le da hambre");
				
				this.conseguir(this.recs.sSillas, "silla");
				this.conseguir(this.recs.sPalillos[this.num], "palillo" + this.num);
				this.conseguir(this.recs.sPalillos[(this.num + 1) % 5], "palillo" + ((this.num + 1) % 5));
				
				Padding.mostrarMsgDeThreadActual("come");
				Thread.sleep(1000);
				Padding.mostrarMsgDeThreadActual("termina de comer");
				
				this.liberar(this.recs.sPalillos[this.num], "palillo" + this.num);
				this.liberar(this.recs.sPalillos[(this.num + 1) % 5], "palillo" + ((this.num + 1) % 5));
				this.liberar(this.recs.sSillas, "silla");
			}
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void conseguir(Semaphore sem, String desc) throws InterruptedException {
		Padding.mostrarMsgDeThreadActual("espera " + desc);
		sem.acquire();
		Padding.mostrarMsgDeThreadActual("obtiene " + desc);
	}

	private void liberar(Semaphore sem, String desc) {
		Padding.mostrarMsgDeThreadActual("libera " + desc);
		sem.release();
	}

}
