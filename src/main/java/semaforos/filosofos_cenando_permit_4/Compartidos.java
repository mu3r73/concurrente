package semaforos.filosofos_cenando_permit_4;

import java.util.concurrent.Semaphore;

public class Compartidos {
	
	public Semaphore sSillas;
	public Semaphore[] sPalillos;
	
	public Compartidos() {
		super();
		this.sSillas = new Semaphore(4);
		
		this.sPalillos = new Semaphore[5];
		for (int i = 0; i < sPalillos.length; i++) {
			this.sPalillos[i] = new Semaphore(1);
		}
	}
	
}
