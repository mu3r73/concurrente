package semaforos.filosofos_cenando_permit_4;

import gen.Padding;

/*
 * ojo: si los filósofos corren en loop infinito, nunca termina
 */
public class Main {
	
	public static void main(String[] args) {
		Compartidos recs = new Compartidos();
		
		for (int i = 0; i < 5; i++) {
			new Thread(new RFilosofo(i, recs), Padding.mkNombreConPadding("filósofo", i)).start();
		}
	}
	
}
