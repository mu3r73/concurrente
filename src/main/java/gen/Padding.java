package gen;

public class Padding {
	
	private static final boolean PADDING_ENABLED = false;
	
	private static final int NUM_PADDING_SPACES = 3;
	private static final String PADDING_CHAR = " ";
	
	private static String mkPadding(int num) {
		return new String(new char[num * Padding.NUM_PADDING_SPACES]).replace("\0", Padding.PADDING_CHAR);
	}
	
	public static String mkNombreConPadding(String nombre, int num) {
		if (Padding.PADDING_ENABLED) {
			return Padding.mkPadding(num) + Padding.mkNombre(nombre, num);
		} else {
			return Padding.mkNombre(nombre, num);
		}
	}
	
	private static String mkNombre(String nombre, int num) {
		return nombre + num;
	}

	public static void mostrarMsg(String nombre, String msg) {
		System.out.println(nombre + ": " + msg);
	}
	
	public static void mostrarMsgDeThreadActual(String msg) {
		Padding.mostrarMsg(Thread.currentThread().getName(), msg);
	}
	
}
