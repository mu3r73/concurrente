package threads.ej08;

public class RParticipante implements Runnable {
	
	private String nombre;
	private int veces;
	private Dado dado;

	public RParticipante(String nombre, int veces, int caras) {
		super();
		this.nombre = nombre;
		this.veces = veces;
		this.dado = new Dado(caras);
	}

	@Override
	public void run() {
		int total = 0;
		int num;
		for (int i = 0; i < this.veces; i++) {
			num = this.dado.tirar();
			System.out.println(this.nombre + " - tirada de mi dado: " + num);
			total += num;
		}
		System.out.println(this.nombre + " - total: " + total);
	}

}
