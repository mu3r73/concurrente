package threads.ej08;

public class Main {
	
	public static void main(String[] args) {
		System.out.println("main: inicio");
		
		Thread[] ps = new Thread[5];
		
		for (int i = 0; i < ps.length; i++) {
			ps[i] = new Thread(new RParticipante("p" + (i + 1), 5, 6));
		}
		
		for (int i = 0; i < ps.length; i++) {
			ps[i].start();
		}
		
		System.out.println("main: fin");
	}

}
