package threads.ej01;

public class Main {

	public static void main(String[] args) {
		System.out.println("main: inicio");
		
		Thread t = new MiThread(10);
		
		t.start();
		
		System.out.println("main: fin");
	}
	
}
