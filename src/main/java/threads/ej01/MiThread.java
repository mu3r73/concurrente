package threads.ej01;

public class MiThread extends Thread {
	
	private int veces;
	
	public MiThread(int veces) {
		super();
		this.veces = veces;
	}

	@Override
	public void run() {
		for (int i = 0; i < this.veces; i++) {
			System.out.println("thread");
		}
	}

}
