package threads.ej06;

public class Main {

	public static void main(String[] args) {
		System.out.println("main: inicio");

		new Thread(new RImpares(10));
		new Thread(new RPares(10));
		new Thread(new RPrimos(10));

		System.out.println("main: fin");
	}

}
