package threads.ej06;

public class RPrimos implements Runnable {

	private int max;

	public RPrimos(int max) {
		super();
		this.max = max;
		new Thread(this).start();
	}

	@Override
	public void run() {
		for (int i = 1; i <= this.max; i++) {
			if (this.esPrimo(i)) {
				System.out.println("RPrimos: " + i);
			}
		}
	}

	private boolean esPrimo(int num) {
		if (num < 2) {
			return false;
		}
		if (num == 2 || num == 3) {
			return true;
		}
		if (num % 2 == 0 || num % 3 == 0) {
			return false;
		}
		for (int i = 6; i <= Math.sqrt(num) + 1; i += 6) {
			if (num % (i - 1) == 0 || num % (i + 1) == 0) {
				return false;
			}
		}
		return true;
	}

}
