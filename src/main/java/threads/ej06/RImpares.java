package threads.ej06;

public class RImpares implements Runnable {
	
	private int max;
	
	public RImpares(int max) {
		super();
		this.max = max;
		new Thread(this).start();
	}

	@Override
	public void run() {
		for (int i = 1; i <= this.max; i++) {
			if (this.esImpar(i)) {
				System.out.println("RImpares: " + i);
			}
		}
	}
	
	private boolean esImpar(int num) {
		return num % 2 == 1;
	}

}
