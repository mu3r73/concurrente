package threads.ej06;

public class RPares implements Runnable {
	
	private int max;

	public RPares(int max) {
		super();
		this.max = max;
		new Thread(this).start();
	}

	@Override
	public void run() {
		for (int i = 1; i <= this.max; i++) {
			if (this.esPar(i)) {
				System.out.println("RPares: " + i);
			}
		}
	}
	
	private boolean esPar(int num) {
		return num % 2 == 0;
	}

}
