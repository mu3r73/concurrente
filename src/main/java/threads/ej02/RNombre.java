package threads.ej02;

public class RNombre implements Runnable {

	private String nombre;

	public RNombre(String nombre) {
		super();
		this.nombre = nombre;
	}

	@Override
	public void run() {
		System.out.println("nombre: " + this.nombre);
	}
		
}
