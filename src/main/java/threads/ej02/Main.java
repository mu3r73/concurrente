package threads.ej02;

public class Main {
	
	public static void main(String[] args) {
		System.out.println("main: inicio");
		
		Thread ts[] = new Thread[5];
		
		for (int i = 0; i < ts.length; i++) {
			ts[i] = new Thread(new RNombre("r" + (i + 1)));
		}
		
		for (int i = 0; i < ts.length; i++) {
			ts[i].start();
		}
		
		System.out.println("main: fin");
	}

}
