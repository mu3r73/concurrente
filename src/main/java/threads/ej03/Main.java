package threads.ej03;

import java.util.Random;

public class Main {
	
	public static void main(String[] args) {
		System.out.println("main: inicio");
		
		Random rng = new Random();
		
		Thread ts[] = new Thread[5];
		
		for (int i = 0; i < ts.length; i++) {
			ts[i] = new Thread(new RNombreNVeces("r" + (i + 1), 5 + rng.nextInt(10)));
		}
		
		for (int i = 0; i < ts.length; i++) {
			ts[i].start();
		}
		
		System.out.println("main: fin");
	}

}
