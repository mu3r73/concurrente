package threads.ej03;

public class RNombreNVeces implements Runnable {
	
	private String nombre;
	private int veces;
	
	public RNombreNVeces(String nombre, int veces) {
		super();
		this.nombre = nombre;
		this.veces = veces;
	}

	@Override
	public void run() {
		for (int i = 0; i < this.veces; i++) {
			System.out.println("nombre: " + this.nombre);
		}
	}
	
}
