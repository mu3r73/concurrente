package threads.ej09;

import java.util.Random;

public class Main {
	
	private static Thread[] crearCorredores(int totCs) {
		Thread[] cs = new Thread[totCs];
		for (int i = 0; i < cs.length; i++) {
			cs[i] = new TCompetidor("c" + (i + 1), 100);
		}
		return cs;
	}

	private static void iniciarCarrera(Thread[] cs) {
		for (int i = 0; i < cs.length; i++) {
			cs[i].start();
		}
	}

	public static void main(String[] args) {
		System.out.println("main: inicio");

		Random rng = new Random();

		Thread[] cs = Main.crearCorredores(4 + rng.nextInt(6)); // entre 4 y 9 competidores
		
		Main.iniciarCarrera(cs);
		
		System.out.println("main: fin");
	}

}
