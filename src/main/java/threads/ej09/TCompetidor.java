package threads.ej09;

public class TCompetidor extends Thread {
	
	private String nombre;
	private Dado dado;
	private int cargasNitro;
	private int distMax;
	private int distRecorrida;

	public TCompetidor(String nombre, int distMax) {
		super();
		this.nombre = nombre;
		this.dado = new Dado(10);
		this.cargasNitro = 2;
		this.distMax = distMax;
		this.distRecorrida = 0;
	}

	@Override
	public void run() {
		int num;
		while (this.distRecorrida < this.distMax) {
			num = this.dado.tirar();
			switch (num) {
				case 2:
					this.demorar(3);
					break;
				
				case 6:
					this.bajarPrioridad();
					break;
				
				case 7:
					break;
				
				case 9:
					this.avanzarConNitro();
					break;
	
				default:
					this.avanzar();
					break;
			}
		}
		System.out.println(this.nombre + ": *** llegada a meta ***");
	}

	private void demorar(int s) {
		try {
			System.out.println(this.nombre + ": demorado " + s + "s -- " + this.getRecorridos());
			Thread.sleep(s * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void bajarPrioridad() {
		int newPri = Math.max(this.getPriority() - 1, Thread.MIN_PRIORITY);
		this.setPriority(newPri);
		System.out.println(this.nombre + ": motor recalentado, prioridad reducida a " + newPri + " -- " + this.getRecorridos());
	}

	private void avanzarConNitro() {
		if (this.cargasNitro > 0) {
			this.distRecorrida += 3;
			this.cargasNitro--;
			System.out.println(this.nombre + ": avance con nitro 3km -- " + this.getRecorridos());
		} else {
			System.out.println(this.nombre + ": nitro sin cargas -- " + this.getRecorridos());
		}
	}

	private void avanzar() {
		this.distRecorrida++;
		System.out.println(this.nombre + ": avance 1 km -- " + this.getRecorridos());
	}
	
	private String getRecorridos() {
		return "recorridos: " + this.distRecorrida + "/" + this.distMax + "km";
	}

}
