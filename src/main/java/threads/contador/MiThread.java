package threads.contador;

public class MiThread extends Thread {
	
	private int times;

	public MiThread(int times) {
		super();
		this.times = times;
	}

	@Override
	public void run() {
		for (int i = 0; i < this.times; i++) {
			Contador.val++;
		}
	}
	
}
