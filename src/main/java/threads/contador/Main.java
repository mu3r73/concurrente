package threads.contador;

public class Main {
	
	public static void main(String[] args) {
		System.out.println("main: inicio");
		
		Thread t1 = new MiThread(3000);
		Thread t2 = new MiThread(5000);
		
		t1.start();
		t2.start();
		
		while (t1.isAlive() || t2.isAlive()) {
			System.out.println(Contador.val);
		}
		
		System.out.println("main: fin");
	}
	
}
