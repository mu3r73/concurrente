package threads.ej05;

public class RAutonomo implements Runnable {
	
	private int total;
	
	public RAutonomo(int total) {
		super();
		this.total = total;
		new Thread(this).start();
	}

	@Override
	public void run() {
		for (int i = 0; i < this.total; i++) {
			System.out.println("RAutonomo: " + (i + 1));
		}
	}

}
