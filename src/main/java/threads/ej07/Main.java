package threads.ej07;

public class Main {
	
	public static void main(String[] args) {
		System.out.println("main: inicio");
		
		Thread tVigilado = new Thread(new RVigilado());
		Thread tVigilante = new Thread(new RVigilante(tVigilado));
		
//		tVigilado.start();
		tVigilante.start();
		
		System.out.println("main: fin");
	}

}

/*
 * Pregunta: Si se ejecuta retiradamente el programa, algunas veces aparece el estado NEW y otras no;
 * todos los hilos pasan siempre por ese estado, entonces, ¿por qué el vigilante a veces no se da cuenta? 
 *
 * R: si vigilado empieza antes que vigilante, vigilante no alcanza a ver su estado NEW
 * (programa modificado para que vigilante siempre vea el NEW) 
*/
