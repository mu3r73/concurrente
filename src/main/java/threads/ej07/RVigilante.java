package threads.ej07;

import java.lang.Thread.State;

public class RVigilante implements Runnable {

	private Thread vigilado;

	public RVigilante(Thread vigilado) {
		super();
		this.vigilado = vigilado;
	}

	@Override
	public void run() {
		State prev = null;
		State curr = this.vigilado.getState();

		this.vigilado.start();
		
		while (this.vigilado.isAlive()) {
			if (curr != prev) {
				System.out.println("RVigilante: " + curr.name());
			}
			prev = curr;
			curr = this.vigilado.getState();
		}
		System.out.println("RVigilante: " + curr.name());
	}

}
