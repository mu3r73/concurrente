package threads.ej07;

public class RVigilado implements Runnable {

	@Override
	public void run() {
		try {
			Thread.sleep(5);
			for (int i = 0; i < 100000; i++) {
				// nada
			}
			Thread.yield();
			for (int i = 0; i < 100000; i++) {
				// nada
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
