package threads.ej10;

import java.util.ArrayList;
import java.util.List;

import gen.Padding;

public class TCajero extends Thread {
	
	private List<Cliente> clientes;
	private int cantProductos;
	
	public TCajero(String nombre) {
		super(nombre);
		this.clientes = new ArrayList<>();
	}
	
	public void agregarCliente(Cliente cliente) {
		this.clientes.add(cliente);
	}
	
	@Override
	public void run() {
		while (!this.clientes.isEmpty()) {
			this.atenderCliente();
		}
	}

	private void atenderCliente() {
		try {
			Cliente cliente = this.clientes.remove(0);
			Padding.mostrarMsgDeThreadActual("atendiendo al cliente " + cliente.getNombre());
			
			this.cantProductos = 0;		
			while(cliente.quedanProductosEnChango()) {
				Padding.mostrarMsgDeThreadActual("pasando 1 producto por el lector de códigos");
				cliente.getProductoDeChango();
				Thread.sleep(1000);
				this.cantProductos++;
			}
			Padding.mostrarMsgDeThreadActual("cantidad de productos cobrados = " + this.cantProductos);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
