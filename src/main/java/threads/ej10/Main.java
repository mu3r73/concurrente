package threads.ej10;

import java.util.Random;

import gen.Padding;

public class Main {
	
	static Random rng = new Random();

	private static TCajero[] crearCajas(int cant) {
		TCajero[] cs = new TCajero[cant];
		for (int i = 0; i < cs.length; i++) {
			cs[i] = new TCajero(Padding.mkNombreConPadding("caja", i + 1));
		}
		return cs;
	}
	
	private static Cliente[] crearClientes(int cant) {
		Cliente[] cs = new Cliente[cant];
		for (int i = 0; i < cs.length; i++) {
			cs[i] = new Cliente("cli" + (i + 1), 5 + rng.nextInt(10));
		}
		return cs;
	}

	private static void ubicarClientesEnCajas(TCajero[] cajas, Cliente[] clis) {
		for (int i = 0; i < cajas.length; i++) {
			cajas[i].agregarCliente(clis[i]);
		}
	}

	private static void atenderClientes(Thread[] cajas) {
		for (int i = 0; i < cajas.length; i++) {
			cajas[i].start();
		}
	}

	public static void main(String[] args) {
		System.out.println("main: inicio");
		
		TCajero[] cajas = Main.crearCajas(3);
		Cliente[] clis = Main.crearClientes(3);
		
		Main.ubicarClientesEnCajas(cajas, clis);
		Main.atenderClientes(cajas);
		
		System.out.println("main: fin");
	}

}
