package threads.ej04;

public class Main {
	
	public static void main(String[] args) {
		System.out.println("main: inicio");
				
		Thread tp = new TPares(10);
		Thread ti = new Thread(new RImpares(10));
		
		tp.start();
		ti.start();
		
		System.out.println("main: fin");
	}

}
