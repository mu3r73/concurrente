package threads.ej04;

public class TPares extends Thread {
	
	private int max;

	public TPares(int max) {
		super();
		this.max = max;
	}

	@Override
	public void run() {
		for (int i = 1; i <= this.max; i++) {
			if (this.esPar(i)) {
				System.out.println("TPares: " + i);
			}
		}
	}
	
	private boolean esPar(int num) {
		return num % 2 == 0;
	}

}
