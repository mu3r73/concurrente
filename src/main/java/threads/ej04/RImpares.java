package threads.ej04;

public class RImpares implements Runnable {
	
	private int max;
	
	public RImpares(int max) {
		super();
		this.max = max;
	}

	@Override
	public void run() {
		for (int i = 1; i <= this.max; i++) {
			if (this.esImpar(i)) {
				System.out.println("RImpares: " + i);
			}
		}
	}
	
	private boolean esImpar(int num) {
		return num % 2 == 1;
	}

}
