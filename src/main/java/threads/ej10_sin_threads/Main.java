package threads.ej10_sin_threads;

import java.util.Random;

import gen.Padding;
import threads.ej10.Cliente;

public class Main {

	static Random rng = new Random();

	private static Cajero[] crearCajas(int cant) {
		Cajero[] cs = new Cajero[cant];
		for (int i = 0; i < cs.length; i++) {
			cs[i] = new Cajero(Padding.mkNombreConPadding("cajero", i + 1));
		}
		return cs;
	}
	
	private static Cliente[] crearClientes(int cant) {
		Cliente[] cs = new Cliente[cant];
		for (int i = 0; i < cs.length; i++) {
			cs[i] = new Cliente("cliente" + (i + 1), 5 + rng.nextInt(10));
		}
		return cs;
	}

	private static void ubicarClientesEnCajas(Cajero[] cajas, Cliente[] clis) {
		for (int i = 0; i < cajas.length; i++) {
			cajas[i].agregarCliente(clis[i]);
		}
	}

	private static void atenderClientes(Cajero[] cajas) {
		for (int i = 0; i < cajas.length; i++) {
			cajas[i].start();
		}
	}

	public static void main(String[] args) {
		System.out.println("main: inicio");
		
		Cajero[] cajas = Main.crearCajas(3);
		Cliente[] clis = Main.crearClientes(3);
		
		Main.ubicarClientesEnCajas(cajas, clis);
		Main.atenderClientes(cajas);
		
		System.out.println("main: fin");
	}

}
