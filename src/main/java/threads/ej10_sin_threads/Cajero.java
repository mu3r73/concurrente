package threads.ej10_sin_threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import gen.Padding;
import threads.ej10.Cliente;

public class Cajero {

	private String nombre;
	private List<Cliente> clientes;
	private int cantProductos;
	
	public Cajero(String nombre) {
		super();
		this.nombre = nombre;
		this.clientes = new ArrayList<>();
	}
	
	public void agregarCliente(Cliente cliente) {
		this.clientes.add(cliente);
	}
	
	public void start() {
		while (!this.clientes.isEmpty()) {
			this.atenderCliente();
		}
	}

	private void atenderCliente() {
		try {
			Cliente cliente = this.clientes.remove(0);
			Padding.mostrarMsg(this.nombre, "atendiendo al cliente " + cliente.getNombre());
			
			this.cantProductos = 0;		
			while(cliente.quedanProductosEnChango()) {
				Padding.mostrarMsg(this.nombre, "pasando 1 producto por el lector de códigos");
				cliente.getProductoDeChango();
				TimeUnit.SECONDS.sleep(1);
				this.cantProductos++;
			}
			Padding.mostrarMsg(this.nombre, "cantidad de productos cobrados = " + this.cantProductos);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
