package threads.ej10_sin_threads;

public class Cliente {
	
	private String nombre;
	private int cantProd;

	public Cliente(String nombre, int cantProd) {
		super();
		this.nombre = nombre;
		this.cantProd = cantProd;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public boolean quedanProductosEnChango() {
		return this.cantProd > 0;
	}
	
	public void getProductoDeChango() {
		this.cantProd--;
	}

}
